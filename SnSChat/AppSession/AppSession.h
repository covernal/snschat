//
//  AppSession.h
//  SnSChat
//
//  Created by Passion on 3/11/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface AppSession : NSObject
{
    BOOL _isLogined;
    User* _userLogined;
    
}

@property(readwrite) BOOL isLogined;
@property(nonatomic,retain) User* userLogined;

+(id)sharedAppSession;

@end
