//
//  AppSession.m
//  SnSChat
//
//  Created by Passion on 3/11/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import "AppSession.h"
static AppSession* shared = nil;

@implementation AppSession

+(id)sharedAppSession
{
    if(shared == nil)
    {
        
        shared = [[AppSession alloc] init];
        
    }
    return  shared;
    
}

- (instancetype)init
{
    self = [super init];
    
    if (self) {
        shared = self;
        _isLogined = NO;
    }
    return self;
}

#pragma user method
-(BOOL)isLogined
{
    NSUserDefaults* pref = [NSUserDefaults standardUserDefaults];
    NSDictionary* dic = [pref valueForKey:@"session"];
    if(dic != nil)
    {
        return true;
    }
    else
    {
        return false;
    }
    return false;
}
-(void)setIsLogined:(BOOL)isLogined
{
    _isLogined = isLogined;
    
}
-(User*)userLogined
{
    return _userLogined;
}
-(void)setUserLogined:(User *)userLogined
{
    _userLogined = userLogined;
}

@end
