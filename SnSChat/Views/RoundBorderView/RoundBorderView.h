//
//  RoundBorderView.h
//  SnSChat
//
//  Created by Passion on 3/12/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoundBorderView : UIView
{
    CGFloat _cornerRadius;
    CGFloat _borderWidth;
    UIColor *_borderColor;
    UIColor *_backColor;
}

@property (readwrite) CGFloat cornerRadius;
@property (readwrite) CGFloat borderWidth;
@property (nonatomic, retain) UIColor *borderColor;
@property (nonatomic, retain) UIColor *backColor;

@end
