//
//  RoundBorderView.m
//  SnSChat
//
//  Created by Passion on 3/12/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import "RoundBorderView.h"

@implementation RoundBorderView

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initView];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView{
    self.backColor = [UIColor whiteColor];
    self.borderColor = [UIColor colorWithRed:194.0/255.0 green:194.0/255.0 blue:194.0/255.0 alpha:1.0];
    self.borderWidth = 2.0f;
    self.cornerRadius = 10.0f;
}

#pragma mark - Getter and Setter Methods
-(CGFloat)cornerRadius{
    return _cornerRadius;
}
-(void)setCornerRadius:(CGFloat)cornerRadius{
    _cornerRadius = cornerRadius;
    
    self.layer.cornerRadius = cornerRadius;
}

-(CGFloat)borderWidth{
    return _borderWidth;
}
-(void)setBorderWidth:(CGFloat)borderWidth{
    _borderWidth = borderWidth;
    
    self.layer.borderWidth = borderWidth;
}

-(UIColor *)borderColor{
    return _borderColor;
}
-(void)setBorderColor:(UIColor *)borderColor{
    _borderColor = borderColor;
    
    self.layer.borderColor = borderColor.CGColor;
}

-(UIColor *)backColor{
    return _backColor;
}
-(void)setBackColor:(UIColor *)backColor{
    _backColor = backColor;
    
    self.backgroundColor = backColor;
}

@end
