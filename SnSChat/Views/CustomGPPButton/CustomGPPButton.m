//
//  CustomGPPButton.m
//  SnSChat
//
//  Created by Passion on 3/17/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import "CustomGPPButton.h"

@implementation CustomGPPButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)awakeFromNib
{
    [self setBackgroundImage:[UIImage imageNamed:@"icon_Google+_a.png"] forState:UIControlStateNormal];
}
@end
