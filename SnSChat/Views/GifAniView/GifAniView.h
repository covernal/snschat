//
//  GifAniView.h
//  SnSChat
//
//  Created by Passion on 3/13/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImage+animatedGIF.h"

@interface GifAniView : UIImageView
{
    NSString* strGifUrl;
}

-(void)loadWithGifStrUrl:(NSString*)strUrl;
-(void)refresh;


@end
