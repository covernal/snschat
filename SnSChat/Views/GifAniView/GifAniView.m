//
//  GifAniView.m
//  SnSChat
//
//  Created by Passion on 3/13/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import "GifAniView.h"
#import "UIImage+animatedGIF.h"

@implementation GifAniView

- (instancetype)init
{
    self = [super init];
    if (self) {
        strGifUrl = @"Loading-Image-Smaller";
        NSURL * url = [[NSBundle mainBundle] URLForResource:strGifUrl withExtension:@"gif"];

        self.image = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url]];
        
        
    }
    return self;
}

-(void)loadWithGifStrUrl:(NSString *)strUrl
{
    strGifUrl = strUrl;
    [self refresh];

}
-(void)refresh
{
    NSURL * url = [[NSBundle mainBundle] URLForResource:strGifUrl withExtension:@"gif"];
    
    self.image = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url]];
}

@end
