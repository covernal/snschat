//
//  DateSelPickerVC.m
//  SnSChat
//
//  Created by Passion on 3/16/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import "DateSelPickerVC.h"

@interface DateSelPickerVC ()

@end

@implementation DateSelPickerVC

-(id)initVC{
    self = [super initWithNibName:@"DateSelPickerVC" bundle:[NSBundle mainBundle]];
    if (self) {
        
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [self initControls];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma  mark - user define
-(void)initControls
{
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    NSDate* date = [self.datePicker date];
    [formatter setDateFormat:@"yyyy/MM/dd"];
    NSString* str = [formatter stringFromDate:date];
    [self.label_Date setText:str];
    
}

#pragma mark - controls
- (IBAction)onChangePicker:(id)sender {
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    NSDate* date = [self.datePicker date];
    [formatter setDateFormat:@"yyyy/MM/dd"];
    NSString* str = [formatter stringFromDate:date];
    [self.label_Date setText:str];

}
- (IBAction)onSetting:(id)sender {
    
    [self.delegate onDatePicker:self.label_Date.text];
    
}

@end
