//
//  DateSelPickerVC.h
//  SnSChat
//
//  Created by Passion on 3/16/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DateSelDelegate <NSObject>

@optional
-(void)onDatePicker:(NSString*) dateString;

@end

@interface DateSelPickerVC : UIViewController

@property(nonatomic,weak) id<DateSelDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UILabel *label_Date;

-(id)initVC;
@end
