//
//  ComboCell.h
//  SnSChat
//
//  Created by Passion on 3/13/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ComboCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *text_Label;

@end
