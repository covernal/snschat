//
//  User.m
//  SnSChat
//
//  Created by Passion on 3/13/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import "User.h"
#import "JSON.h"

@implementation User

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        [self setLogin:@""];
        [self setFullName:@""];
        [self setEmail:@""];
        [self setPassword:@""];
        [self setBirthday:@""];
        [self setGender:@""];
        [self setFacebookId:@""];
        [self setTwitterId:@""];
        [self setGoogleId:@""];
        [self setUrlAvata:@""];
        [self setTags:@""];
        
        
    }
    return self;
}
-(User*)initWithData:(QBUUser *)qUser
{
    self = [super init];
    if(self)
    {
        NSLog(@"%@",qUser);
        
        [self setLogin:qUser.login];
        [self setFullName:qUser.fullName];
        [self setPassword:qUser.password];
        [self setEmail:qUser.email];
        [self setFacebookId:qUser.facebookID];
        [self setTwitterId:qUser.twitterID];
        [self setTags:qUser.tags];
        [self setTimeCreated:qUser.createdAt];
        [self setTimeLast:qUser.lastRequestAt];
        
        NSString* customData = qUser.customData;
        NSDictionary* json = [customData JSONValue];
        if(json!=nil){
            NSString* pBirth = [json valueForKey:@"birthday"];
            NSString* pGender = [json valueForKey:@"gender"];
            NSString* pUrlAvata = [json valueForKey:@"url"];
            
            [self setBirthday:pBirth];
            [self setGender:pGender];
            [self setUrlAvata:pUrlAvata];
        }
        
        
    }
    
    return self;
    
}

-(NSString*)idUser
{
    return _idUser;
}
-(void)setIdUser:(NSString*)idUser
{
    _idUser = idUser;
}

-(NSString*)login{
    return _login;
}
-(void)setLogin:(NSString *)login
{
    _login = login;
}

-(NSString*)fullName
{
    return _fullName;
}
-(void)setFullName:(NSString *)fullName
{
    _fullName = fullName;
}
-(NSString*)password
{
    return _password;
}
-(void)setPassword:(NSString *)password
{
    _password = password;
}
-(NSString*)email
{
    return _email;
}
-(void)setEmail:(NSString *)email
{
    _email = email;
}

-(NSString*)facebookId
{
    return _facebookId;
}


-(void)setFacebookId:(NSString *)facebookId
{
    _facebookId = facebookId;
}

-(NSString*)twitterId
{
    return _twitterId;
}

-(void)setTwitterId:(NSString *)twitterId
{
    _twitterId = twitterId;
}
-(NSString*)googleId
{
    return _googleId;
}
-(void)setGoogleId:(NSString *)googleId
{
    _googleId = googleId;
}
-(NSString*)tags
{
    return _tags;
}
-(void)setTags:(NSString *)tags
{
    _tags = tags;
}
-(NSDate*)timeCreated{
    return _timeCreated;
    
}
-(void)setTimeCreated:(NSDate *)timeCreated
{
    _timeCreated = timeCreated;
}
-(NSDate*)timeLast
{
    return _timeLast;
}
-(void)setTimeLast:(NSDate *)timeLast
{
    _timeLast = timeLast;
}

-(NSString*)birthday
{
    return _birthday;
}
-(void)setBirthday:(NSString *)birthday
{
    _birthday = birthday;
}
-(NSString*)gender
{
    return _gender;
}
-(void)setGender:(NSString *)gender
{
    _gender = gender;
}
-(NSString*)urlAvata
{
    return _urlAvata;
}
-(void)setUrlAvata:(NSString *)urlAvata
{
    _urlAvata = urlAvata;
}

#pragma mark - session
-(void)saveSession{
    NSMutableDictionary* dic = [NSMutableDictionary dictionary];
    [dic setValue:_idUser forKey:@"iduser"];
    [dic setValue:_login forKey:@"login"];
    [dic setValue:_fullName forKey:@"fullname"];
    [dic setValue:_password forKey:@"password"];
    [dic setValue:_email forKey:@"email"];
    [dic setValue:_facebookId forKey:@"facebookid"];
    [dic setValue:_twitterId forKey:@"twitterid"];
    [dic setValue:_googleId forKey:@"googleid"];
    [dic setValue:_tags forKey:@"tags"];
    [dic setValue:_birthday forKey:@"birthday"];
    [dic setValue:_gender forKey:@"gender"];
    [dic setValue:_urlAvata forKey:@"url"];
    
    NSUserDefaults* prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:dic forKey:@"session"];
    
    NSLog(@"Saving Session Success! = %@",dic);
    
}
+(User*)retriveSessionUser
{
    User* user = [User new];
    
    NSMutableDictionary* dic = [[NSUserDefaults standardUserDefaults] valueForKey:@"session"];
    if(dic == nil)
    {
        return nil;
    }
    [user setIdUser:[dic valueForKey:@"iduser"]];
    [user setLogin:[dic valueForKey:@"login"]];
    [user setFullName:[dic valueForKey:@"fullname"]];
    [user setPassword:[dic valueForKey:@"password"]];
    [user setEmail:[dic valueForKey:@"email"]];
    [user setFacebookId:[dic valueForKey:@"facebookid"]];
    [user setTwitterId:[dic valueForKey:@"twitterid"]];
    [user setGoogleId:[dic valueForKey:@"googleid"]];
    [user setTags:[dic valueForKey:@"tags"]];
    [user setBirthday:[dic valueForKey:@"birthday"]];
    [user setGender:[dic valueForKey:@"gender"]];
    [user setUrlAvata:[dic valueForKey:@"url"]];
    
    NSLog(@"Retriving SessionUser Success!","");
    return  user;
    
}
+(User*)cloneObjectWith:(User *)user
{
    User* pUser =[User new];
    [pUser setIdUser:[user idUser]];
    [pUser setLogin:[user login]];
    [pUser setFullName:[user fullName]];
    [pUser setPassword:[user password]];
    [pUser setEmail:[user email]];
    [pUser setBirthday:[user birthday]];
    [pUser setGender:[user gender]];
    [pUser setFacebookId:[user facebookId]];
    [pUser setTwitterId:[user twitterId]];
    [pUser setGoogleId:[user googleId]];
    [pUser setUrlAvata:[user urlAvata]];
    [pUser setTags:[user tags]];
    return pUser;
    
}
-(void)registerToQuickBlox
{
    QBUUser* quser = [QBUUser user];
    quser.login = [self login];
    quser.fullName = [self fullName];
    quser.password = [self password];
    
    quser.email = [self email];//shopsocial7@gmail.com
//    quser.facebookID = [self facebookId];
//    quser.twitterID = [self twitterId];
//    quser.externalUserID = [[ self googleId] integerValue];
    //user.tags = [self tags];
    
    NSMutableDictionary* dicCustom = [NSMutableDictionary dictionary];
    [dicCustom setObject:[self birthday] forKey:@"birthday"];
    [dicCustom setObject:[self gender] forKey:@"gender"];
    [dicCustom setObject:[self urlAvata] forKey:@"url"];
    
    NSString*strCustomData = [dicCustom JSONFragment];
    
    quser.customData = strCustomData;
    
    NSLog(@"%@",quser);
    
    [QBRequest signUp:quser successBlock:^(QBResponse *response, QBUUser *user) {
        [self.delegate registerToQuickBlox:YES object:user];
        
        
    } errorBlock:^(QBResponse *response) {
        [self.delegate registerToQuickBlox:NO object:nil];
        
    }];
    
}

-(void)registerToQBFromSocial:(NSInteger)type
{
    QBUUser* quser = [QBUUser user];
    quser.fullName = [self fullName];

    if(type == 1)
    {
        quser.login = [self facebookId];
        quser.password =[self facebookId]; //[self facebookId];
        quser.facebookID = [self facebookId];
    }
    else if(type == 2)
    {
        quser.login = [self twitterId];
        quser.password = [self twitterId];
        quser.twitterID = [self twitterId];
    }
    else if(type == 3)
    {
        quser.login = [self googleId];
        quser.password = [self googleId];
        quser.externalUserID = [[self googleId] integerValue];
        
    }
    NSMutableDictionary* dicCustom = [NSMutableDictionary dictionary];
    [dicCustom setObject:[self birthday] forKey:@"birthday"];
    [dicCustom setObject:[self gender] forKey:@"gender"];
    [dicCustom setObject:[self urlAvata] forKey:@"url"];
    
    NSString*strCustomData = [dicCustom JSONFragment];
    
    quser.customData = strCustomData;
    
    NSLog(@"%@",quser);
    
    [QBRequest signUp:quser successBlock:^(QBResponse *response, QBUUser *user) {
        User* loginedUser = [[User new] initWithData:user];
        [loginedUser saveSession];
        
        [self.delegate registerToQuickBlox:YES object:user];
        
    } errorBlock:^(QBResponse *response) {
        [self.delegate registerToQuickBlox:NO object:nil];
        
    }];
}
@end
