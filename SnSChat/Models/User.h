//
//  User.h
//  SnSChat
//
//  Created by Passion on 3/13/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UserDelegate <NSObject>
@optional
-(void)registerToQuickBlox:(BOOL)success object:(id)user;

@end


@interface User : NSObject
{
    NSString* _idUser;
    NSString* _login;
    NSString* _fullName;
    NSString* _password;
    NSString* _email;
    NSString* _facebookId;
    NSString* _twitterId;
    NSString* _googleId;
    NSString* _tags;
    NSDate* _timeLast;
    NSDate* _timeCreated;
    
    NSString* _birthday;
    NSString* _gender;
    NSString* _urlAvata;
    
}
@property(nonatomic,weak) id<UserDelegate> delegate;
@property(nonatomic) NSString* idUser;
@property(nonatomic,retain) NSString* login;
@property(nonatomic,retain) NSString* fullName;
@property(nonatomic,retain) NSString* password;
@property(nonatomic,retain) NSString* email;
@property(nonatomic,retain) NSString* facebookId;
@property(nonatomic,retain)NSString* twitterId;
@property(nonatomic,retain) NSString* googleId;
@property(nonatomic,retain) NSString* tags;
@property(nonatomic,retain) NSDate* timeLast;
@property(nonatomic,retain) NSDate* timeCreated;

@property(nonatomic,retain) NSString* birthday;
@property(nonatomic,retain) NSString* gender;
@property(nonatomic,retain) NSString* urlAvata;

-(User*)initWithData:(QBUUser*)qUser;
-(void)saveSession;
-(void)registerToQuickBlox;
-(void)registerToQBFromSocial:(NSInteger)type;
+(User*)cloneObjectWith:(User*)user;
+(User*)retriveSessionUser;

@end
