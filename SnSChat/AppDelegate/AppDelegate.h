//
//  AppDelegate.h
//  SnSChat
//
//  Created by Passion on 3/10/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate >

@property (strong, nonatomic) UIWindow *window;


@end

