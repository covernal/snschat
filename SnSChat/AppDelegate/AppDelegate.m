//
//  AppDelegate.m
//  SnSChat
//
//  Created by Passion on 3/10/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import "AppDelegate.h"
#import "SplashVC.h"
#import "Constants.h"
#import <GooglePlus/GooglePlus.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [GPPSignIn sharedInstance].clientID = ClientID_Google;
    [GPPDeepLink setDelegate:self];
    [GPPDeepLink readDeepLinkAfterInstall];
    
    
    //Facebook;
    [FBProfilePictureView class];
    [FBLoginView class];
 

    // Override point for customization after application launch.
    [[QBApplication sharedApplication] setApplicationId:20591];
    [QBConnection registerServiceKey:@"GzNLC8xOCnAzsLD"];
    [QBConnection registerServiceSecret:@"6Ar4uFu7q5hZ75E"];
    [QBSettings setAccountKey:@"4pwY7nU5yidFJm6zAxaL"];
    
    [QBRequest createSessionWithSuccessBlock:^(QBResponse *response, QBASession *session) {
        // Success, You have got Application session, now READ something.
        
    } errorBlock:^(QBResponse *response) {
        
        // error handling
        NSLog(@"error: %@", response.error);
    }];
    
    [self showSplashVC];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBAppCall handleDidBecomeActive];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    NSLog(@"---%@", url);
    
    //return [FBSession.activeSession handleOpenURL:url];
    BOOL wasHandled = [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
    if (wasHandled) return wasHandled;
    
    // You can add your app-specific url handling code here if needed
    
    if([GPPURLHandler handleURL:url sourceApplication:sourceApplication annotation:annotation])
    {
        wasHandled = true;
    }
    else{
        wasHandled = false;
    }
    
    return wasHandled;
}

#pragma user function
-(void)showSplashVC
{
    SplashVC* splashVC = [[SplashVC alloc] initWithNibName:@"SplashVC" bundle:nil];
    UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:splashVC];
    [nav setNavigationBarHidden:YES];
    
    self.window.rootViewController = nav;
    
    [self.window makeKeyAndVisible];
}

#pragma mark - GPPDeepLinkDelegate

- (void)didReceiveDeepLink:(GPPDeepLink *)deepLink {
    // An example to handle the deep link data.
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Deep-link Data"
                          message:[deepLink deepLinkID]
                          delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil];
    [alert show];
}



@end
