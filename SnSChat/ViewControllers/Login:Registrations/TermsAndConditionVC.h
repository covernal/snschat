//
//  TermsAndConditionVC.h
//  SnSChat
//
//  Created by Passion on 3/14/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GifAniView.h"

@interface TermsAndConditionVC : UIViewController
{
    NSString* htmlString;
}

@property (weak, nonatomic) IBOutlet GifAniView *img_bkg;

@property (weak, nonatomic) IBOutlet UILabel *label_Title;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

- (IBAction)onBack:(id)sender;
-(id)initWithLocalUrl:(NSString*)urlLocal title:(NSString*)title;
@end
