//
//  TermsAndConditionVC.m
//  SnSChat
//
//  Created by Passion on 3/14/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import "TermsAndConditionVC.h"

@interface TermsAndConditionVC ()

@end

@implementation TermsAndConditionVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initWithLocalUrl:(NSString*)urlLocal title:(NSString *)title
{
    self = [super initWithNibName:@"TermsAndConditionVC" bundle:[NSBundle mainBundle] ];
    if (self) {
        NSString *htmlFile = [[NSBundle mainBundle] pathForResource:urlLocal ofType:@"html"];
        htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
   // [self.img_bkg loadWithGifStrUrl:@"Loading-Image-Smaller"];
    [self.webView loadHTMLString:htmlString baseURL:nil];
    
}

- (IBAction)onBack:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}



@end
