//
//  RegisterChoiceVC.m
//  SnSChat
//
//  Created by Passion on 3/12/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import "RegisterChoiceVC.h"
#import "UIImage+animatedGIF.h"
#import "RegisterVC.h"
#import "TermsAndConditionVC.h"

@interface RegisterChoiceVC ()
@property(nonatomic,retain) RegisterVC* vcRegister;

@end

@implementation RegisterChoiceVC

+(id)vcLogin{
    return [[RegisterChoiceVC alloc] initWithNibName:@"RegisterChoiceVC" bundle:[NSBundle mainBundle]];
}



-(id)initVC{
    self = [super initWithNibName:@"RegisterChoiceVC" bundle:[NSBundle mainBundle]];
    if (self) {
        
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        nState = 0;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.vcRegister = [[RegisterVC alloc] initVC];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [self initControls];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    nState = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button click define
- (IBAction)onButtonClick:(id)sender {
    NSInteger tag = [sender tag];
    switch (tag) {
        case 1://nShop Social
            self.vcRegister.delegate = self;
            [self.navigationController pushViewController:self.vcRegister animated:NO];
            
            break;
        case 2://facebook
            
            break;
        case 3://twitter
            
            break;
        case 4://google +
            
            break;
        case 5://terms and conditions
            
            break;
        case 6://back button
            [self.navigationController popViewControllerAnimated:NO];
            break;
            
        default:
            break;
    }
}
- (IBAction)onTermsAndConditions:(id)sender {
    TermsAndConditionVC* vcWeb =[[TermsAndConditionVC alloc] initWithLocalUrl:@"Shopnsocial Terms and Conditions HTML 09032015" title:@"Terms & Conditions"];
    [self.navigationController pushViewController:vcWeb animated:YES];
}

- (IBAction)onPrivacyPolicies:(id)sender {
    TermsAndConditionVC* vcWeb =[[TermsAndConditionVC alloc] initWithLocalUrl:@"Privacy Policy HTML" title:@"Privacy Policies"];
    [self.navigationController pushViewController:vcWeb animated:YES];
}


#pragma mark -  init Setting
-(void)initControls
{
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"Loading-Image-Smaller" withExtension:@"gif"];
    self.imageView.image = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url]];
    if(nState == 1)
    {
        self.viewFailReport.hidden = false;
    }
    else{
        self.viewFailReport.hidden = true;
    }
    
}

#pragma mark - RegisterVC Delegate
-(void)onFailRegister
{
    nState = 1;
    [self initControls];
}

@end
