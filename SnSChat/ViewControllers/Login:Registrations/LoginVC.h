//
//  LoginVC.h
//  SnSChat
//
//  Created by Passion on 3/10/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import <FacebookSDK/FacebookSDK.h>
#import  <Twitter/Twitter.h>
#import <Social/Social.h>
#import "FHSTwitterEngine.h"
#import <GooglePlus/GooglePlus.h>
#import <GoogleOpenSource/GoogleOpenSource.h>
#import "CustomGPPButton.h"
#import "DataPickerState.h"
#import "RegisterVC.h"
#import "PwdForgottenVC.h"
#import "UnForgottenVC.h"
#import "SocialRegisterVC.h"
@class GPPSignInButton;

@interface LoginVC : UIViewController<UITextFieldDelegate,UIAlertViewDelegate,FBLoginViewDelegate,FHSTwitterEngineAccessTokenDelegate,GPPSignInDelegate,RegisterVCDelegate,PwdForgottenVCDelegate,UnForgottenVCDelegate,SocialRegisterVCelegate>
{
    STATE_LOGINVC state;
    BOOL IsSrollUp;
    CGFloat UpView;
    NSInteger nFailCount;
    
    // States storing the current set of selected elements for each data picker.(google +)
    DataPickerState *_colorSchemeState;
    DataPickerState *_styleState;
    DataPickerState *_appActivitiesState;
    
    NSMutableArray* arrayActivities;
    
}
@property (weak, nonatomic) IBOutlet CustomGPPButton *signInButton;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property(retain , nonatomic) FBLoginView* loginFBView;
@property (nonatomic, strong) ACAccountStore *account;




- (IBAction)onClickButton:(id)sender;
+(id)vcLogin;
-(id)initVC;

@end
