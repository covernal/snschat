//
//  UnForgottenVC.m
//  SnSChat
//
//  Created by Passion on 3/13/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import "UnForgottenVC.h"
#import "TermsAndConditionVC.h"
#import "PwdForgottenVC.h"

@interface UnForgottenVC ()

@end

@implementation UnForgottenVC

#pragma mark - initvc
-(id)initVC:(NSString*)strEmail{
    self = [super initWithNibName:@"UnForgottenVC" bundle:[NSBundle mainBundle]];
    if (self) {
        self.bnt_Emal.enabled = false;
        if([strEmail isEqualToString:@""])
        {
            self.bnt_Emal.titleLabel.text =@"username@shopnsocial.com";
        }
        else if(strEmail == nil)
        {
            self.bnt_Emal.titleLabel.text =@"username@shopnsocial.com";
        }
        else{
            self.bnt_Emal.titleLabel.text = strEmail;
        }
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}
#pragma mark -load
- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark - button click
- (IBAction)onClickPassword:(id)sender {
    PwdForgottenVC* vcPwd = [[PwdForgottenVC alloc] initVC];
    vcPwd.delegate = self;
    [self.navigationController pushViewController:vcPwd animated:YES];
    
}

- (IBAction)onClickTermsAndConditions:(id)sender {
    TermsAndConditionVC* vcWeb =[[TermsAndConditionVC alloc] initWithLocalUrl:@"Shopnsocial Terms and Conditions HTML 09032015" title:@"Terms & Conditions"];
    [self.navigationController pushViewController:vcWeb animated:YES];
}
- (IBAction)onPrivacyPolicies:(id)sender {
    TermsAndConditionVC* vcWeb =[[TermsAndConditionVC alloc] initWithLocalUrl:@"Privacy Policy HTML" title:@"Privacy Policies"];
    [self.navigationController pushViewController:vcWeb animated:YES];
}





#pragma mark - password reset Success
-(void)onResetPwdSuccess
{
    [self.delegate onResetPwdSuccessFromUserName];
    [self.navigationController popViewControllerAnimated:YES];
    
}
@end
