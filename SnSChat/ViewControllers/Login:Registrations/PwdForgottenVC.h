//
//  PwdForgottenVC.h
//  SnSChat
//
//  Created by Passion on 3/13/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GifAniView.h"

@protocol PwdForgottenVCDelegate <NSObject>

@optional

-(void)onResetPwdSuccess;

@end
@interface PwdForgottenVC : UIViewController<QBActionStatusDelegate,UITextFieldDelegate>
{
    CGFloat UpView;
    BOOL IsSrollUp;
}
@property (weak, nonatomic) IBOutlet UITextField *text_UserNameEmail;
@property (weak, nonatomic) IBOutlet GifAniView *img_bkg;
@property (weak, nonatomic) IBOutlet UIButton *bnt_Login;
@property(weak,nonatomic) id<PwdForgottenVCDelegate> delegate;
-(id)initVC;
@end
