//
//  SocialRegisterVC.h
//  SnSChat
//
//  Created by Passion on 3/12/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DateSelPickerVC.h"
#import "User.h"
#import "RegisterVC.h"

@protocol SocialRegisterVCelegate <NSObject>

@optional
-(void)onFailRegisterFromSocial;
-(void)onSuccessFromSocial;

@end

@interface SocialRegisterVC : UIViewController<UITextFieldDelegate,DateSelDelegate,UITableViewDataSource,UITableViewDelegate,UserDelegate,UIAlertViewDelegate,RegisterVCDelegate>
{
    BOOL IsSrollUp;
    CGFloat UpView;
    User * sUser;
    NSInteger nStateFromSocial;
}
@property(weak,nonatomic)id<SocialRegisterVCelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *bnt_signUp;
@property (weak, nonatomic) IBOutlet UIView *viewComboBox;

- (IBAction)onClickButton:(id)sender;
-(id)initVC:(NSInteger)typeSocial userReg:(User*)user;
@end
