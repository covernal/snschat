//
//  GLoginVC.m
//  SnSChat
//
//  Created by Passion on 3/13/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import "GLoginVC.h"
#import "TermsAndConditionVC.h"
#import "Service.h"
#import "Constants.h"
#import "BrowserHomePageVC.h"
@interface GLoginVC ()

@end

@implementation GLoginVC

-(id)initVC:(User*)userLogined{
    self = [super initWithNibName:@"GLoginVC" bundle:[NSBundle mainBundle]];
    if (self) {
        loginedUser = [User cloneObjectWith:userLogined];
        
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self initControls];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - touch event
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self ScrollDown];
    
    [self keyBoardResign];
}
#pragma mark - textfield delegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
  
    CGRect frameTextField = [textField frame];
    CGPoint point = frameTextField.origin;
    CGPoint pos = [textField convertPoint:point fromView:self.view];
    pos = CGPointMake(-pos.x, -pos.y);
    CGSize sizeDevice = [[Service sharedService] sizeDevice];
    CGFloat height = sizeDevice.height - pos.y - frameTextField.size.height;
    CGFloat heightKeyboard = (CGFloat)KEYBOARD_HEIGHT_IPAD;
    if(height < heightKeyboard + 40)
    {
        CGRect frameBnt = [self.bnt_NewPwdCreate frame];
        
        CGFloat height = sizeDevice.height - frameBnt.origin.y - frameBnt.size.height;
        
        UpView = heightKeyboard - height + 40;
        
        [self ScrollUp];
        
    }
    
    return true;
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    
    return true;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self ScrollDown];
    [self keyBoardResign];
    
    return true;
}

#pragma mark - user define
-(void)initControls
{
    self.bnt_NewPwdCreate.enabled = false;
    
    IsSrollUp = false;
    [self.text_Password setText:@""];
    self.text_Password.delegate = self;
    [self.text_ConfirmPwd setText:@""];
    self.text_ConfirmPwd.delegate = self;
}
-(void)ScrollUp
{
    if(IsSrollUp == NO)
    {
        [UIView animateWithDuration:0.3 animations:^{
            CGRect frame = self.view.frame;
            frame.origin.y = frame.origin.y - UpView;
            [self.view setFrame:frame];
            IsSrollUp = true;
        }];
    }
    IsSrollUp = YES;
}
-(void)ScrollDown
{
    if (IsSrollUp == YES) {
        [UIView animateWithDuration:0.3 animations:^{
            CGRect frame = self.view.frame;
            frame.origin.y = frame.origin.y + UpView;
            [self.view setFrame:frame];
            IsSrollUp = false;
        }];
    }
    IsSrollUp = NO;
}
-(void)keyBoardResign
{
    [self.text_ConfirmPwd resignFirstResponder];
    [self.text_Password resignFirstResponder];
    
}
#pragma mark - button click
- (IBAction)onClickCreate:(id)sender {
    
    QBUUser* userTemp = [QBUUser user];
    userTemp.ID = [[loginedUser idUser] integerValue];
    userTemp.oldPassword = [loginedUser password];
    if([self.text_ConfirmPwd.text isEqualToString:[self.text_Password text]])
    {
        userTemp.password = [self.text_ConfirmPwd text];
    }
    else
    {
        UIAlertView* alertView = [[UIAlertView alloc] init];
        [alertView setTitle:@"Check your confirm password"];
        [alertView addButtonWithTitle:@"Yes"];
        [alertView show];
    }
    
    [QBRequest updateUser:userTemp successBlock:^(QBResponse *response, QBUUser *user) {
        // User updated successfully
        
    } errorBlock:^(QBResponse *response) {
        NSString *errorMessage = [[response.error description] stringByReplacingOccurrencesOfString:@"(" withString:@""];
        errorMessage = [errorMessage stringByReplacingOccurrencesOfString:@")" withString:@""];
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Oops!"
                                                              message:errorMessage
                                                             delegate:nil
                                                    cancelButtonTitle:@"Got it"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
    }];
    
    
}

- (IBAction)onClickUserForgotten:(id)sender {
}

- (IBAction)onClickPwdForgotten:(id)sender {
    
}

- (IBAction)onClickBack:(id)sender {
    BrowserHomePageVC* bcBrowser = [[BrowserHomePageVC alloc] initVC];
    [self.navigationController pushViewController:bcBrowser animated:YES];
}
- (IBAction)onTermsAndConditions:(id)sender {
    TermsAndConditionVC* vcWeb =[[TermsAndConditionVC alloc] initWithLocalUrl:@"Shopnsocial Terms and Conditions HTML 09032015" title:@"Terms & Conditions"];
    [self.navigationController pushViewController:vcWeb animated:YES];
}

- (IBAction)onPrivacyPolicies:(id)sender {
    TermsAndConditionVC* vcWeb =[[TermsAndConditionVC alloc] initWithLocalUrl:@"Privacy Policy HTML" title:@"Privacy Policies"];
    [self.navigationController pushViewController:vcWeb animated:YES];
}



@end
