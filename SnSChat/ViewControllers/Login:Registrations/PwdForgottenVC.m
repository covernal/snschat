//
//  PwdForgottenVC.m
//  SnSChat
//
//  Created by Passion on 3/13/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import "PwdForgottenVC.h"
#import "TermsAndConditionVC.h"
#import "Service.h"
#import "Constants.h"
#import "MBProgressHUD.h"
@interface PwdForgottenVC ()

@end

@implementation PwdForgottenVC

#pragma mark - initvc
-(id)initVC{
    self = [super initWithNibName:@"PwdForgottenVC" bundle:[NSBundle mainBundle]];
    if (self) {
        
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    IsSrollUp = false;
    self.text_UserNameEmail.delegate = self;
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - touch event
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self keyBoardResign];
    [self ScrollDown];
    
    
}

#pragma mark - textField Delegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
//     [self.text_UserNameEmail becomeFirstResponder];
    CGRect frameTextField = [textField frame];
    CGPoint point = frameTextField.origin;
    CGPoint pos = [textField convertPoint:point fromView:self.view];
    pos = CGPointMake(-pos.x, -pos.y);
    CGSize sizeDevice = [[Service sharedService] sizeDevice];
    CGFloat height = sizeDevice.height - pos.y - frameTextField.size.height;
    CGFloat heightKeyboard = (CGFloat)KEYBOARD_HEIGHT_IPAD;
    if(height < heightKeyboard + 40)
    {
        CGRect frameBnt = [self.bnt_Login frame];
        
        CGFloat height = sizeDevice.height - frameBnt.origin.y - frameBnt.size.height;
        
        UpView = heightKeyboard - height + 40;
        
        [self ScrollUp];
        
    }
   

    return true;
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    NSString*str =[textField text];
    if([str isEqual:@""])
    {
        return true;
    }
    
    if([[Service sharedService] validateEmail:str])
    {
        return  true;
    }
    else{
        UIAlertView* alertView = [[UIAlertView alloc] init];
        [alertView setTitle:@"Please check the format of your email."];
        [alertView addButtonWithTitle:@"Ok"];
        [alertView show];
        return false;
    }
    return true;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self ScrollDown];
    [self keyBoardResign];
    return true;
}
#pragma mark - user define
-(BOOL)isValid
{
    NSString* strEmailUserName = self.text_UserNameEmail.text;
    if([strEmailUserName isEqualToString:@""])
    {
        return false;
    }
    return true;
}
-(void)keyBoardResign
{
    [self.text_UserNameEmail resignFirstResponder];
}
-(void)sendUE:(NSString*)userEmail
{
    
   if([[Service sharedService] isConectedToGoogle]==NO)
   {
       UIAlertView* alertView =[[UIAlertView alloc] init];
       [alertView setTitle:@"No connect to the internet"];
       [alertView addButtonWithTitle:@"Yes"];
       [alertView show];
   }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [QBRequest resetUserPasswordWithEmail:userEmail successBlock:^(QBResponse *response) {
        // Reset was successful
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [self.delegate onResetPwdSuccess];
        [self.navigationController popViewControllerAnimated:NO];
        
        
    } errorBlock:^(QBResponse *response) {
        // Error
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        UIAlertView* alertView =[[UIAlertView alloc] init];
        [alertView setTitle:@"Fail to reset the password"];
        [alertView addButtonWithTitle:@"Yes"];
        [alertView show];
        
    }];
}

-(void)completedWithResult:(QBAAuthSessionCreationResult *)result{
    if(result.success && [result isKindOfClass:QBAAuthSessionCreationResult.class]){
        //success
        
        // reset password
        [QBUsers resetUserPasswordWithEmail:self.text_UserNameEmail.text delegate:self];
        
    }
    else{
        UIAlertView* alertView = [[UIAlertView alloc] init];
        [alertView setTitle:@"Already it had been reseted."];
        [alertView addButtonWithTitle:@"Ok"];
        [alertView show];
    }
//    else if (result.success && [result isKindOfClass:Result.class]){
//        // you did it
//    }
}
-(void)ScrollUp
{
    if(IsSrollUp == NO)
    {
        [UIView animateWithDuration:0.3 animations:^{
            CGRect frame = self.view.frame;
            frame.origin.y = frame.origin.y - UpView;
            [self.view setFrame:frame];
            IsSrollUp = true;
        }];
    }
}
-(void)ScrollDown
{
    if (IsSrollUp == YES) {
        [UIView animateWithDuration:0.3 animations:^{
            CGRect frame = self.view.frame;
            frame.origin.y = frame.origin.y + UpView;
            [self.view setFrame:frame];
            IsSrollUp = false;
        }];
    }
}

#pragma mark - button click

- (IBAction)onClickSend:(id)sender {
    if([self isValid]== false)
    {
        
        UIAlertView* alertView = [[UIAlertView alloc] init];
        [alertView setTitle:@"Please input your email."];
        [alertView addButtonWithTitle:@"Ok"];
        [alertView show];
        return;
    }
    
    else{
        if([[Service sharedService] validateEmail:self.text_UserNameEmail.text])
        {
             [self sendUE:self.text_UserNameEmail.text];
        }
        else{
            UIAlertView* alertView = [[UIAlertView alloc] init];
            [alertView setTitle:@"The email should be like a email format."];
            [alertView addButtonWithTitle:@"Ok"];
            [alertView show];
        }
    }
   
    
    
}

- (IBAction)onClickUserName:(id)sender {
}

- (IBAction)onBack:(id)sender {
    
    [self.navigationController popViewControllerAnimated:NO];
    
}
- (IBAction)onTermsAndConditions:(id)sender {
    TermsAndConditionVC* vcWeb =[[TermsAndConditionVC alloc] initWithLocalUrl:@"Shopnsocial Terms and Conditions HTML 09032015" title:@"Terms & Conditions"];
    [self.navigationController pushViewController:vcWeb animated:YES];
}
- (IBAction)onPrivacyPolicies:(id)sender {
    TermsAndConditionVC* vcWeb =[[TermsAndConditionVC alloc] initWithLocalUrl:@"Privacy Policy HTML" title:@"Privacy Policies"];
    [self.navigationController pushViewController:vcWeb animated:YES];
}



@end
