//
//  RegisterVC.m
//  SnSChat
//
//  Created by Passion on 3/11/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import "RegisterVC.h"
#import "Service.h"
#import "Constants.h"
#import "ComboCell.h"
#import "JSON.h"
#import "MBProgressHUD.h"
#import "TermsAndConditionVC.h"
#import "DateSelPickerVC.h"
#import "BrowserHomePageVC.h"
#import "User.h"

@interface RegisterVC ()
@property (weak, nonatomic) IBOutlet UIView *textInvolveView;
@property (weak, nonatomic) IBOutlet UIButton *bnt_Register;

@property(retain,nonatomic) UITableView* tableView;
@property(retain,nonatomic) NSMutableArray* arrayDataSource;

@property(retain,nonatomic) DateSelPickerVC* vcDatePicker;

@end

@implementation RegisterVC



-(id)initVC{
    self = [super initWithNibName:@"RegisterVC" bundle:[NSBundle mainBundle]];
    if (self) {
        
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerNib:[UINib nibWithNibName:@"ComboCell" bundle:nil] forCellReuseIdentifier:@"ComboCell"];
    [self initComboBox];
    
    self.vcDatePicker = [[DateSelPickerVC alloc] initVC];
    
    [self addChildViewController:self.vcDatePicker];
    [self.view addSubview:self.vcDatePicker.view];
    self.vcDatePicker.delegate = self;
    
    self.vcDatePicker.view.hidden = true;
    
    [self initControls];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
}

-(void)viewWillDisappear:(BOOL)animated{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
#pragma mark - Touch Event
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    [self ScrollDown];
    
    [self keyBoardResign];
    
}
#pragma mark - textfield delegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField == self.text_Gender)
    {
        [self comboChange];
        return false;
    }
    if(textField == self.text_birthday)
    {
        [self showDateSelVC];
        return false;
    }
    CGRect frameTextField = [textField frame];
    CGPoint point = frameTextField.origin;
    CGPoint pos = [textField convertPoint:point fromView:self.view];
    pos = CGPointMake(-pos.x, -pos.y);
    CGSize sizeDevice = [[Service sharedService] sizeDevice];
    CGFloat height = sizeDevice.height - pos.y - frameTextField.size.height;
    CGFloat heightKeyboard = (CGFloat)KEYBOARD_HEIGHT_IPAD;
    if(height < heightKeyboard + 40)
    {
        CGRect frameBnt = [self.bnt_Register frame];
        
        CGFloat height = sizeDevice.height - frameBnt.origin.y - frameBnt.size.height;
        UpView = heightKeyboard - height + 30;
        //        UpView = 330;
        [self ScrollUp];
    }
    
    return true;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self ScrollDown];
    [self keyBoardResign];
    return true;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if(textField == self.text_Password || textField == self.text_Confirmpass)
    {
        NSString* strInput = textField.text;
        NSInteger length = [strInput length];
        if(length == 0)
        {
            return  true;
        }
        else if(length < 8)
        {
            UIAlertView* alertView = [[UIAlertView alloc] init];
            alertView.title = @"The password must be owned with over 8 characters.";
            [alertView addButtonWithTitle:@"Yes"];
            [alertView show];
            
        }
    }
    else if(textField == self.text_Email)
    {
        NSString* strEmail = textField.text;
        BOOL checkValidEmail = [[Service sharedService] validateEmail:strEmail];
        if(checkValidEmail == YES)
        {
            return true;
        }
        else{
            UIAlertView* alertView = [[UIAlertView alloc] init];
            alertView.title = @"The email must be like a email format";
            [alertView addButtonWithTitle:@"Yes"];
            [alertView show];
        }
    }
    return true;
}
-(void)keyBoardResign
{
    [self.text_UserName resignFirstResponder];
    [self.text_Email resignFirstResponder];
    [self.text_Password resignFirstResponder];
    [self.text_Confirmpass resignFirstResponder];
    [self.text_birthday resignFirstResponder];
    [self.text_Gender resignFirstResponder];
}
#pragma mark - tableview delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.text_Gender.frame.size.height;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ComboCell* cell = [[[NSBundle mainBundle] loadNibNamed:@"ComboCell" owner:self options:nil] objectAtIndex:0];
    NSString* str = [self.arrayDataSource objectAtIndex:indexPath.row];
    cell.text_Label.text = str;
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* str = [self.arrayDataSource objectAtIndex:indexPath.row];
    self.text_Gender.text = str;
    [self comboChange];
}
#pragma mark -user define
-(void)initControls
{
    self.text_UserName.text = @"";
    self.text_UserName.delegate = self;
    self.text_Email.text = @"";
    self.text_Email.delegate = self;
    self.text_Password.text = @"";
    self.text_Password.delegate = self;
    self.text_Confirmpass.text = @"";
    self.text_Confirmpass.delegate = self;
    self.text_birthday.text = @"";
    self.text_birthday.delegate = self;
    self.text_Gender.text = @"";
    self.text_Gender.delegate = self;
    
    self.bnt_AgreeCheck.selected = false;
    IsSrollUp = false;
    
    
    
    
}
-(void)initComboBox
{
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.arrayDataSource = [NSMutableArray array];
    [self.arrayDataSource addObject:@"male"];
    [self.arrayDataSource addObject:@"female"];
    [self.tableView reloadData];
    [self.tableView setSeparatorColor:[UIColor colorWithRed:255 green:255 blue:255 alpha:1]];
    [self.tableView setBackgroundColor:[UIColor colorWithRed:245 green:245 blue:245 alpha:1]];
    [self.viewComboBox addSubview:self.tableView];
    self.tableView.hidden = true;
    self.tableView.bounces = false;
    
    CGRect frameTable;
    CGRect frameGender = [self.viewComboBox frame];
    frameTable = CGRectMake(0, frameGender.size.height, frameGender.size.width , self.text_Gender.frame.size.height * 2);
    [self.tableView setFrame:frameTable];
    
    
}

-(void)registerNShop
{
    [self keyBoardResign];
    [self ScrollDown];
    UIAlertView* alertView = [[UIAlertView alloc] init];
    if(![self.text_Password.text isEqualToString:self.text_Confirmpass.text])
    {
        [alertView setTitle:@"Check the password again."];
        [alertView addButtonWithTitle:@"Ok"];
        [alertView show];
        return ;
    }
    else if([self.text_UserName.text isEqualToString:@""])
    {
        [alertView setTitle:@"Input the username"];
        [alertView addButtonWithTitle:@"Ok"];
        [alertView show];
        return ;
    }
    else if([self.text_Email.text isEqualToString:@""])
    {
        
        [alertView setTitle:@"Input the Email"];
        [alertView addButtonWithTitle:@"Ok"];
        [alertView show];
        return ;
    }
    else if([self.text_Password.text isEqualToString:@""])
    {
        
        [alertView setTitle:@"Input password"];
        [alertView addButtonWithTitle:@"Ok"];
        [alertView show];
        return ;
    }
    else if([self.text_Confirmpass.text isEqualToString:@""])
    {
        
        [alertView setTitle:@"Input confirm password"];
        [alertView addButtonWithTitle:@"Ok"];
        [alertView show];
        return ;
    }
    else if([self.text_birthday.text isEqualToString:@""])
    {
        
        [alertView setTitle:@"Input birthday."];
        [alertView addButtonWithTitle:@"Ok"];
        [alertView show];
        return ;
    }
    else if([self.text_Gender.text isEqualToString:@""])
    {
        
        [alertView setTitle:@"Input gender"];
        [alertView addButtonWithTitle:@"Ok"];
        [alertView show];
        return ;
    }
    else if(self.bnt_AgreeCheck.selected != true)
    {
        [alertView setTitle:@"Agree with the terms and conditions "];
        [alertView addButtonWithTitle:@"Ok"];
        [alertView show];
        return ;
    }
    
    else{
        User* pUser = [User new];
        [pUser setLogin:self.text_UserName.text];
        [pUser setEmail:self.text_Email.text];
        [pUser setPassword:self.text_Password.text];
        [pUser setBirthday:self.text_birthday.text];
        [pUser setGender:self.text_Gender.text];
        pUser.delegate = self;
        [pUser registerToQuickBlox];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
    }
}
-(void)regitsterByFacebook
{
    
}

-(void)registerByTwitter
{
    
}
-(void)registerByGoogle
{
    
}
-(void)viewTermsandConditions
{
    TermsAndConditionVC* vcWeb =[[TermsAndConditionVC alloc] initWithLocalUrl:@"Shopnsocial Terms and Conditions HTML 09032015" title:@"Terms & Conditions"];
    [self.navigationController pushViewController:vcWeb animated:YES];
}

-(void)ScrollUp
{
    if(IsSrollUp == NO)
    {
        [UIView animateWithDuration:0.3 animations:^{
            CGRect frame = self.view.frame;
            frame.origin.y = frame.origin.y - UpView;
            [self.view setFrame:frame];
            IsSrollUp = true;
        }];
    }
}
-(void)ScrollDown
{
    if (IsSrollUp == YES) {
        [UIView animateWithDuration:0.3 animations:^{
            CGRect frame = self.view.frame;
            frame.origin.y = frame.origin.y + UpView;
            [self.view setFrame:frame];
            IsSrollUp = false;
        }];
    }
}

-(void)buttonCheck
{
    self.bnt_AgreeCheck.selected = !self.bnt_AgreeCheck.selected;
}
-(void)comboChange
{
    
    [self.tableView reloadData];
    if(self.bnt_combo.selected == false)
    {
        
        [UIView animateWithDuration: 0.2f delay:0.1f options:UIViewAnimationCurveEaseOut animations:^{
            CGRect frame = self.viewComboBox.frame;
            CGFloat height = frame.size.height * 3.0f;
            frame.size.height = height;
            [self.viewComboBox setFrame:frame];
            
            
        }completion:^(BOOL finished){
            NSLog(@"Done!");
            self.tableView.hidden = false;
            self.bnt_combo.selected = !self.bnt_combo.selected;
        }];
        
    }
    else{
        
        [UIView animateWithDuration: 0.2f delay:0.1f options:UIViewAnimationCurveEaseOut animations:^{
            CGRect frame = self.viewComboBox.frame;
            CGFloat height = frame.size.height / 3.0f;
            frame.size.height = height;
            [self.viewComboBox setFrame:frame];
            self.bnt_combo.selected = !self.bnt_combo.selected;
            
        }completion:^(BOOL finished){
            NSLog(@"Done!");
            self.tableView.hidden = true;
            
        }];
        
    }
    
    
}
-(void)showDateSelVC
{
    CGRect frameDatePicker = [self.vcDatePicker.view frame];
    CGSize sizeDevice = [[Service sharedService] sizeDevice];
    
    frameDatePicker = CGRectMake((sizeDevice.width-frameDatePicker.size.width)/2.0f, (sizeDevice.height-frameDatePicker.size.height)/2.0f, frameDatePicker.size.width, frameDatePicker.size.height);
    [self.vcDatePicker.view setFrame:frameDatePicker];
    [self.vcDatePicker.view setHidden:NO];
}
-(BOOL)isInvalid
{
    UIAlertView* alertView = [[UIAlertView alloc] init];
    
    
    if(![self.text_Password.text isEqualToString:self.text_Confirmpass.text])
    {
        [alertView setTitle:@"Check the password again."];
        [alertView addButtonWithTitle:@"Ok"];
        [alertView show];
        return false;
    }
    else if([self.text_UserName.text isEqualToString:@""])
    {
        [alertView setTitle:@"Input the username"];
        [alertView addButtonWithTitle:@"Ok"];
        [alertView show];
        return false;
    }
    else if([self.text_Email.text isEqualToString:@""])
    {
        
        [alertView setTitle:@"Input the Email"];
        [alertView addButtonWithTitle:@"Ok"];
        [alertView show];
        return false;
    }
    else if([self.text_Password.text isEqualToString:@""])
    {
        
        [alertView setTitle:@"Input password"];
        [alertView addButtonWithTitle:@"Ok"];
        [alertView show];
        return false;
    }
    else if([self.text_Confirmpass.text isEqualToString:@""])
    {
        
        [alertView setTitle:@"Input confirm password"];
        [alertView addButtonWithTitle:@"Ok"];
        [alertView show];
        return false;
    }
    else if([self.text_birthday.text isEqualToString:@""])
    {
        
        [alertView setTitle:@"Input birthday."];
        [alertView addButtonWithTitle:@"Ok"];
        [alertView show];
        return false;
    }
    else if([self.text_Gender.text isEqualToString:@""])
    {
        
        [alertView setTitle:@"Input gender"];
        [alertView addButtonWithTitle:@"Ok"];
        [alertView show];
        return false;
    }
    else if(self.bnt_AgreeCheck.selected != true)
    {
        [alertView setTitle:@"Agree with the terms and conditions "];
        [alertView addButtonWithTitle:@"Ok"];
        [alertView show];
        return false;
    }
    
    
    return true;
    
}

#pragma mark - Button Click Events
- (IBAction)onClickButton:(id)sender {
    NSInteger tag = [sender tag];
    switch (tag) {
        case 1:
            [self registerNShop];
            break;
        case 2:
            [self regitsterByFacebook];
            break;
        case 3:
            [self registerByTwitter];
            break;
        case 4:
            [self registerByGoogle];
            break;
        case 5:
            [self viewTermsandConditions];
            break;
        case 6:
            [self.navigationController popViewControllerAnimated:NO];
            
            break;
        case 7:
            [self buttonCheck];
            break;
        case 8:
            [self comboChange];
            break;
            
        default:
            break;
    }
}
- (IBAction)onCalendar:(id)sender {
    
    
    [self keyBoardResign];
    [self ScrollDown];
    
    [self showDateSelVC];
    
}
- (IBAction)onTermsConditions:(id)sender {
    TermsAndConditionVC* vcWeb =[[TermsAndConditionVC alloc] initWithLocalUrl:@"Shopnsocial Terms and Conditions HTML 09032015" title:@"Terms & Conditions"];
    [self.navigationController pushViewController:vcWeb animated:YES];
}

#pragma mark - UiAlertView Delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSInteger tag = [alertView tag];
    NSString* title = [alertView buttonTitleAtIndex:buttonIndex];
    if(tag == 1)
    {
        if([title isEqualToString:@"Ok"])
        {
            
            BrowserHomePageVC* vcBrowser = [[BrowserHomePageVC alloc] initVC];
            [self.navigationController pushViewController:vcBrowser animated:YES];
        }
        
    }
    if(tag == 10)
    {
        if([title isEqualToString:@"Ok"])
        {
            [self.delegate onSuccess];
            
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    
}
#pragma DatePickerSel Delegate
-(void)onDatePicker:(NSString *)dateString
{
    self.text_birthday.text = dateString;
    [self.vcDatePicker.view setHidden:YES];
    
}
#pragma mark - User Registration Delegate
-(void)registerToQuickBlox:(BOOL)success object:(id)user
{
    if(success == true)
    {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        UIAlertView* alertView = [[UIAlertView alloc] init];
        [alertView setTitle:@"Success to sign up."];
        [alertView addButtonWithTitle:@"Ok"];
        alertView.tag = 10;
        alertView.delegate = self;
        [alertView show];
        
        
    }
    else
    {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        UIAlertView* alertView = [[UIAlertView alloc] init];
        [alertView setTitle:@"Fail to sign up."];
        [alertView addButtonWithTitle:@"Yes"];
        [alertView show];
        
    }
}
@end
