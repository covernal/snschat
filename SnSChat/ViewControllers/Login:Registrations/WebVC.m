//
//  WebVC.m
//  DYB
//
//  Created by Passion on 2/24/15.
//  Copyright (c) 2015 albert. All rights reserved.
//

#import "WebVC.h"

#import "MBProgressHUD.h"

@interface WebVC ()


@end

@implementation WebVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithUrl:(NSString*)urlWeb{
    
    self = [super initWithNibName:@"TermsAndConditionVC" bundle:[NSBundle mainBundle] ];
    if (self) {
        self.url = urlWeb;
        [self loadPage:self.url];
    }
    return self;
}
-(id)initWithLocalUrl:(NSString*)urlLocal
{
    self = [super initWithNibName:@"TermsAndConditionVC" bundle:[NSBundle mainBundle] ];
    if (self) {
        NSString *htmlFile = [[NSBundle mainBundle] pathForResource:urlLocal ofType:@"html"];
        NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
        
        [self.uiWebView loadHTMLString:htmlString baseURL:nil];
        
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
   
    
    if (_refreshHeaderView == nil) {
        _refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0, 0-self.uiWebView.scrollView.bounds.size.height, self.uiWebView.scrollView.frame.size.width, self.uiWebView.scrollView.bounds.size.height)];
        _refreshHeaderView.delegate = self;
        [self.uiWebView.scrollView addSubview:_refreshHeaderView];
    }
    [_refreshHeaderView refreshLastUpdatedDate];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.uiWebView setScalesPageToFit:YES];
    
//    [self.img_bkg loadWithGifStrUrl:@"Loading-Image-Smaller.gif"];
    
    // [self loadPage:self.url];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadPage:(NSString*)url_Page
{
    if(url_Page != nil ){
        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.hud.labelText = @"Loading...";
        [self.hud show:YES];
        NSURL *url = [[NSURL alloc] initWithString:url_Page];
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
        self.uiWebView.delegate = self;
        [self.uiWebView loadRequest: request];
        
        
    }
    else{

    }
    
}



- (IBAction)onBack:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}
#pragma mark - webview delegate

- (void)webViewDidStartLoad:(UIWebView *)webView {
    _reloading = YES;
}
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    
    _reloading = NO;
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.uiWebView.scrollView];
    [self.hud hide:YES];
    
   
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    NSLog(@"load page error:%@", [error description]);
    _reloading = NO;
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.uiWebView.scrollView];
    [self.hud hide:YES];
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
    
    //  [self loadPage:self.url];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
    
    return _reloading; // should return if data source model is reloading
    
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view{
    
    return [NSDate date]; // should return date data source was last changed
    
}


@end
