//
//  LoginVC.m
//  SnSChat
//
//  Created by Passion on 3/10/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import "LoginVC.h"
#import "UIImage+animatedGIF.h"
#import "UnForgottenVC.h"
#import "PwdForgottenVC.h"
#import "AppSession.h"
#import "User.h"
#import "Service.h"
#import "TermsAndConditionVC.h"
#import "MBProgressHUD.h"
#import "RegisterVC.h"
#import "BrowserHomePageVC.h"
#import <FacebookSDK/FacebookSDK.h>
#import  <Twitter/Twitter.h>
#import <Social/Social.h>
#import "FHSTwitterEngine.h"
#import <Accounts/Accounts.h>
#import "SocialRegisterVC.h"
#import "Constants.h"
#import "AgreeTermCondVC.h"
#import <QuartzCore/QuartzCore.h>

@interface LoginVC ()
@property (weak, nonatomic) IBOutlet UIView *viewInput;
@property (weak, nonatomic) IBOutlet UITextField *text_EmailorUname;
@property (weak, nonatomic) IBOutlet UITextField *text_Password;
@property (weak, nonatomic) IBOutlet UIView *viewUnEmail;
@property (weak, nonatomic) IBOutlet UIView *viewPwd;


@property (weak, nonatomic) IBOutlet UIButton *bnt_Login;
@property (weak, nonatomic) IBOutlet UIButton *bnt_Register;

@property (weak, nonatomic) IBOutlet UILabel *labelInputTitle;

@property (weak, nonatomic) IBOutlet UILabel *labelState;
@property (weak, nonatomic) IBOutlet UIView *viewState;

@end

@implementation LoginVC
@synthesize account;
@synthesize signInButton;

+(id)vcLogin{
    return [[LoginVC alloc] initWithNibName:@"LoginVC" bundle:[NSBundle mainBundle]];
}

-(id)initVC{
    self = [super initWithNibName:@"LoginVC" bundle:[NSBundle mainBundle]];
    if (self) {
        state = LOGIN_RIGHTALL;
        [self initGoogleEnable];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    nFailCount = 0;
    
    //    self.loginFBView = [[FBLoginView alloc] initWithReadPermissions:
    //                        @[@"public_profile", @"email", @"user_friends"]];
    [[FHSTwitterEngine sharedEngine]permanentlySetConsumerKey:Twitter_consumer_key andSecret:Twitter_consumer_secret];
    [[FHSTwitterEngine sharedEngine]setDelegate:self];
    
    [[FHSTwitterEngine sharedEngine] loadAccessToken];
}
-(void)viewWillAppear:(BOOL)animated
{
    [self adoptUserSettings];
    
    [self initControls];
    //    [[GPPSignIn sharedInstance] trySilentAuthentication];
    //    [GPPSignIn sharedInstance].shouldFetchGoogleUserID = YES;
    //    [GPPSignIn sharedInstance].attemptSSO = YES;
    [super viewWillAppear:animated];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [self ScrollDown];
    [self keyBoardResign];
    
    [self initControls];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - touch event
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self ScrollDown];
    
    [self keyBoardResign];
}
#pragma mark - textfield delegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(state ==LOGIN_LOCKED)
    {
        return false;
    }
    
    CGRect frameTextField = [textField frame];
    CGPoint point = frameTextField.origin;
    CGPoint pos = [textField convertPoint:point fromView:self.view];
    pos = CGPointMake(-pos.x, -pos.y);
    CGSize sizeDevice = [[Service sharedService] sizeDevice];
    CGFloat height = sizeDevice.height - pos.y - frameTextField.size.height;
    CGFloat heightKeyboard = (CGFloat)KEYBOARD_HEIGHT_IPAD;
    if(height < heightKeyboard + 40)
    {
        CGRect frameBnt = [self.bnt_Login frame];
        
        CGFloat height = sizeDevice.height - frameBnt.origin.y - frameBnt.size.height;
        
        UpView = heightKeyboard - height + 40;
        
        [self ScrollUp];
        
    }
    
    return true;
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    
    return true;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self ScrollDown];
    [self keyBoardResign];
    
    return true;
}

#pragma mark - Facebook Related
-(void)loginByFacBook
{
    
    //Facebook Login
    [FBSession openActiveSessionWithReadPermissions:@[@"email", @"user_location"]
                                       allowLoginUI:YES
                                  completionHandler:
     ^(FBSession *session, FBSessionState state, NSError *error) {
         [self sessionStateChanged:session state:state error:error];
     }
     ];
    
}
- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error
{
    // If the session was opened successfully
    if (!error && state == FBSessionStateOpen){
        NSLog(@"Session opened, Permission = %@",FBSession.activeSession.permissions);
        // Show the user the logged-in UI
        [self getUserFromFB];
        
        return;
    }
    if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed){
        // If the session is closed
        NSLog(@"Session closed");
        // Show the user the logged-out UI
        
        return;
    }
    
    // Handle errors
    if (error){
        NSLog(@"Error");
        NSString *alertText;
        NSString *alertTitle;
        // If the error requires people using an app to make an action outside of the app in order to recover
        if ([FBErrorUtility shouldNotifyUserForError:error] == YES){
            alertTitle = @"Something went wrong";
            alertText = [FBErrorUtility userMessageForError:error];
            //     [self showMessage:alertText withTitle:alertTitle];
        } else {
            
            // If the user cancelled login, do nothing
            if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
                NSLog(@"User cancelled login");
                
                // Handle session closures that happen outside of the app
            } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession){
                alertTitle = @"Session Error";
                alertText = @"Your current session is no longer valid. Please log in again.";
                
                //        [self showMessage:alertText withTitle:alertTitle];
                
                // For simplicity, here we just show a generic message for all other errors
                // You can learn how to handle other errors using our guide: https://developers.facebook.com/docs/ios/errors
            } else {
                //Get more error information from the error
                NSDictionary *errorInformation = [[[error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"] objectForKey:@"body"] objectForKey:@"error"];
                
                // Show the user an error message
                alertTitle = @"Something went wrong";
                alertText = [NSString stringWithFormat:@"Please retry. \n\n If the problem persists contact us and mention this error code: %@", [errorInformation objectForKey:@"message"]];
                //  [self showMessage:alertText withTitle:alertTitle];
            }
        }
        // Clear this token
        [FBSession.activeSession closeAndClearTokenInformation];
        // Show the user the logged-out UI
        // [self userLoggedOut];
    }
}
-(void)getUserFromFB{//1376907279299884
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [[FBRequest requestForMe] startWithCompletionHandler:
     ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
         if (!error) {
             
             NSLog(@"Logged in user: %@", user);
             
             FBAccessTokenData *accessTokenData = [[FBSession activeSession] accessTokenData];
             NSLog(@"Access token: %@", accessTokenData.accessToken);
             
             NSString *userID = [user objectForKey:@"id"];
             NSString *userName = [user objectForKey:@"name"];
             NSString *birthday = [user objectForKey:@"birthday"];
             NSString *gender = [user objectForKey:@"gender"];
             NSString *email = [user valueForKey:@"email"];
             
             User* user = [User new];
             [user setFacebookId:userID];
             [user setLogin:userName];
             [user setEmail:email];
             [user setBirthday:birthday];
             [user setGender:gender];
             [self performSelector:@selector(onFBSetUp:) withObject:user afterDelay:0.1f];
             
         }
     }];
}
-(void)onFBSetUp:(id)object
{
    User* userGet = (User*)object;
    NSLog((@"Facebook ID = %@",[userGet facebookId]));
    
    [QBRequest usersWithFacebookIDs:@[[userGet facebookId]] page:[QBGeneralResponsePage responsePageWithCurrentPage:1 perPage:10] successBlock:^(QBResponse *response, QBGeneralResponsePage *page, NSArray *users) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        if([users count]==0)
        {
            [self gotoCompleteRegVC:1 user:userGet];
        }
        else
        {
            User*pUser = [[User alloc] init];
            
            [pUser initWithData:[users objectAtIndex:0]];
            [pUser saveSession];
            [self goToNShopHomePage];
            
            
        }
    } errorBlock:^(QBResponse *response) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
    }];
    
}
#pragma mark - Twitter Related
-(void)loginByTwitter
{
    UIViewController *loginController = [[FHSTwitterEngine sharedEngine]loginControllerWithCompletionHandler:^(BOOL success)
                                         {
                                             if (success)
                                             {
                                                 NSLog(@"Twitter login success");
                                                 
                                                 [self getUserFromTwitter];
                                                 
                                             }
                                             else
                                             {
                                                 NSLog(@"Twitter login failure");
                                                 
                                                 UIAlertView* alertView = [[UIAlertView alloc] init];
                                                 [alertView setTitle:@"Fail to login via Twitter."];
                                                 [alertView addButtonWithTitle:@"Ok"];
                                                 [alertView show];
                                             }
                                         }];
    
    
    [self presentViewController:loginController animated:YES completion:nil];
    
}



#pragma mark -Social
-(void)getUserFromTwitter//3083780350
{
    NSString* twitterId =[[Service sharedService] getUserIdFromAccessToken:[self loadAccessToken]];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
    [QBRequest usersWithTwitterIDs:@[twitterId] page:[QBGeneralResponsePage responsePageWithCurrentPage:1 perPage:10] successBlock:^(QBResponse *response, QBGeneralResponsePage *page, NSArray *users) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        if([users count] == 0)
        {
            User* user =[User new];
            [user setTwitterId:twitterId];
            [self gotoCompleteRegVC:2 user:user];
            
        }
        else
        {
            QBUUser* user = [users objectAtIndex:0];
            User* pUser = [User new];
            [pUser initWithData:user];
            [pUser saveSession];
            [self goToNShopHomePage];
        }
        
    } errorBlock:^(QBResponse *response) {
        [MBProgressHUD hideAllHUDsForView:self.view  animated:YES];
        
    }];
    
    
    
}

-(void)gotoCompleteRegVC:(NSInteger)typeSocial user:(User*)user
{
    SocialRegisterVC* vcSocialRegister = [[SocialRegisterVC alloc] initVC:typeSocial userReg:user];
    vcSocialRegister.delegate = self;
    [self.navigationController pushViewController:vcSocialRegister animated:true];
    
}

-(void)loginByGoogle
{
    
}
-(void)loginByNShop
{
    if([self isValidate] == false)
    {
        return;
    }
    else
    {
        NSString* strPwd = [self.text_Password text];
        NSString* strUserEmail = [self.text_EmailorUname text];
        MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self.view  animated:YES];
        hud.labelText = @"Sign in...";
        if([[Service sharedService] validateEmail:strUserEmail])//Email;
        {
            [self loginNShopWithEmail:strUserEmail password:strPwd];
        }
        else
        {//USerName;
            [self loginNShopWithUsername:strUserEmail password:strPwd];
        }
        
        
    }
    
}
-(void)loginNShopWithEmail:(NSString*)strEmail password:(NSString*)pwd
{
    
    [QBRequest logInWithUserEmail:strEmail password:pwd successBlock:^(QBResponse *response, QBUUser *user) {
        
        // Success, do something
        NSLog(@"User Email = %@", user.email);
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        nFailCount = 0;
        [[AppSession sharedAppSession] setIsLogined:true];
        User* userLogined = [User new];
        [userLogined initWithData:user];
        [userLogined saveSession];
        [self goToNShopHomePage];
        
    } errorBlock:^(QBResponse *response) {
        // error handling
        NSLog(@"error: %@", response.error);
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        nFailCount++;
        NSInteger nLimitFail = LOGINAVAILABLE;
        if(nFailCount >= 5)
        {
            state = LOGIN_LOCKED;
        }
        else if(nFailCount > 0){
            state = LOGIN_FAIL;
        }
        [self initControls];
    }];
    
}
-(void)loginNShopWithUsername:(NSString*)strUserName password:(NSString*)pwd
{
    
    [QBRequest logInWithUserLogin:strUserName password:pwd successBlock:^(QBResponse *response, QBUUser *user) {
        
        // Success, do something
        NSLog(@"User Info = %@", user);
        
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        nFailCount = 0;
        [[AppSession sharedAppSession] setIsLogined:true];
        User* userLogined = [User new];
        [userLogined initWithData:user];
        [userLogined saveSession];
        [self goToNShopHomePage];
        
    } errorBlock:^(QBResponse *response) {
        // error handling
        NSLog(@"error: %@", response.error);
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        nFailCount++;
        NSInteger nLimitFail = LOGINAVAILABLE;
        if(nFailCount >= 5)
        {
            state = LOGIN_LOCKED;
        }
        else if(nFailCount > 0){
            state = LOGIN_FAIL;
        }
        [self initControls];
    }];
    
}
-(void)goToNShopHomePage
{
//    BrowserHomePageVC* vcBrower = [[BrowserHomePageVC alloc] initVC];
//    [self.navigationController pushViewController:vcBrower animated:YES];
    AgreeTermCondVC* vcAgree = [[AgreeTermCondVC alloc] initVC];
    [self.navigationController pushViewController:vcAgree animated:YES];
    
    
}
-(void)forgottenUserName
{
    UnForgottenVC* vcForgottenUserName = [[UnForgottenVC alloc] initVC:nil];
    vcForgottenUserName.delegate = self;
    [self.navigationController pushViewController:vcForgottenUserName animated:YES];
    
}
-(void)forgottenUserPassword
{
    PwdForgottenVC* vcPwdForgotten = [[PwdForgottenVC alloc] initVC];
    vcPwdForgotten.delegate = self;
    [self.navigationController pushViewController:vcPwdForgotten animated:YES];
    
}
#pragma mark - User Define
-(void)keyBoardResign
{
    [self.text_EmailorUname resignFirstResponder];
    [self.text_Password resignFirstResponder];
    
}
-(void)Register
{
    RegisterVC* vcRegister = [[RegisterVC alloc] initVC];
    vcRegister.delegate = self;
    [self.navigationController pushViewController:vcRegister animated:YES];
    
}

-(BOOL)isValidate
{
    UIAlertView* viewAlert = [[UIAlertView alloc] init];
    
    if([[self.text_EmailorUname text] isEqualToString:@""])
    {
        [viewAlert setTitle:@"Input username."];
        [viewAlert addButtonWithTitle:@"Ok"];
        [viewAlert setTag:1];
        [viewAlert show];
        return false;
    }
    if([[self.text_Password text] isEqualToString:@""])
    {
        [viewAlert setTitle:@"Input your password."];
        [viewAlert addButtonWithTitle:@"Ok"];
        [viewAlert setTag:2];
        [viewAlert show];
        return false;
    }
    return  true;
}
-(void)ScrollUp
{
    if(IsSrollUp == NO)
    {
        [UIView animateWithDuration:0.3 animations:^{
            CGRect frame = self.view.frame;
            frame.origin.y = frame.origin.y - UpView;
            [self.view setFrame:frame];
            IsSrollUp = true;
        }];
    }
    IsSrollUp = YES;
}
-(void)ScrollDown
{
    if (IsSrollUp == YES) {
        [UIView animateWithDuration:0.3 animations:^{
            CGRect frame = self.view.frame;
            frame.origin.y = frame.origin.y + UpView;
            [self.view setFrame:frame];
            IsSrollUp = false;
        }];
    }
    IsSrollUp = NO;
}
#pragma mark - button click
- (IBAction)onClickButton:(id)sender {
    NSInteger tag = [sender tag];
    
    switch (tag) {
        case 1:
            [self loginByFacBook];
            break;
        case 2:
            [self loginByTwitter];
            break;
        case 3:
            [self loginByGoogle];
            break;
        case 4:
            if([[Service sharedService] isConectedToGoogle] == YES)
            {
                [self keyBoardResign];
                [self ScrollDown];
                [self loginByNShop];
            }
            else{
                UIAlertView* alertView = [[Service sharedService] alertView:@"No connecting to internet!" msg:@"" buttons:@[@"Yes"] taget:self];
                [alertView show];
                
            }
            break;
        case 5:
            [self forgottenUserName];
            break;
        case 6:
            [self forgottenUserPassword];
            break;
        case 7:
            [self Register];
            break;
            
        default:
            break;
    }
}
- (IBAction)onRegister:(id)sender {
    RegisterVC* vcRegister = [[RegisterVC alloc] initVC];
    [self.navigationController pushViewController:vcRegister animated:YES];
    
}

- (IBAction)onTermsAndConditions:(id)sender {
    TermsAndConditionVC* vcWeb =[[TermsAndConditionVC alloc] initWithLocalUrl:@"Shopnsocial Terms and Conditions HTML 09032015" title:@"Terms & Conditions"];
    [self.navigationController pushViewController:vcWeb animated:YES];
}
- (IBAction)onPrivaryPolices:(id)sender {
    TermsAndConditionVC* vcWeb =[[TermsAndConditionVC alloc] initWithLocalUrl:@"Privacy Policy HTML" title:@"Privacy Policies"];
    [self.navigationController pushViewController:vcWeb animated:YES];
}

#pragma mark - Init Controls
-(void)initControls
{
    IsSrollUp = false;
    [signInButton setBackgroundImage:[UIImage imageNamed:@"icon_Google+_a.png"] forState:UIControlStateNormal];
    
    [self.text_Password setText:@""];
    self.text_Password.delegate = self;
    [self.text_EmailorUname setText:@""];
    self.text_EmailorUname.delegate = self;
    CGRect frameInput = [self.labelInputTitle frame];
    if(state == LOGIN_RIGHTALL)
    {
        self.viewUnEmail.layer.borderWidth=0;
        self.viewUnEmail.layer.borderColor=[UIColor colorWithRed:245 green:245 blue:245 alpha:1].CGColor;
        self.viewPwd.layer.borderWidth =0;
        self.viewPwd.layer.borderColor=[UIColor colorWithRed:245 green:245 blue:245 alpha:1].CGColor;
        
        [self.labelState setText:@""];
        self.viewState.hidden = YES;
        frameInput.origin.y = 453;
        [self.labelInputTitle setFrame:frameInput];
        
    }
    else if(state == LOGIN_FAIL){
        self.viewUnEmail.layer.borderWidth=2;
        self.viewUnEmail.layer.borderColor=[UIColor colorWithRed:255 green:0 blue:0 alpha:1].CGColor;
        self.viewPwd.layer.borderWidth =2;
        self.viewPwd.layer.borderColor=[UIColor colorWithRed:255 green:0 blue:0 alpha:1].CGColor;
        self.viewState.hidden = NO;
        [self.labelState setText:LOGIN_REPORT_FAIL];
        frameInput.origin.y = 423;
        [self.labelInputTitle setFrame:frameInput];
        
        
    }
    else if(state == LOGIN_LOCKED)
    {
        self.viewUnEmail.layer.borderWidth=2;
        self.viewUnEmail.layer.borderColor=[UIColor colorWithRed:255 green:0 blue:0 alpha:1].CGColor;
        self.viewPwd.layer.borderWidth =2;
        self.viewPwd.layer.borderColor=[UIColor colorWithRed:255 green:0 blue:0 alpha:1].CGColor;
        
        self.viewState.hidden = NO;
        [self.labelState setText:LOGIN_REPORT_LOCK];
        self.labelState.hidden = NO;
        self.bnt_Login.enabled = NO;
        self.bnt_Register.enabled = NO;
        
        frameInput.origin.y = 423;
        [self.labelInputTitle setFrame:frameInput];
        
        
    }
 
}
#pragma mark - RegisterVC Delegate
-(void)onSuccess
{
    [self initControls];
}


-(void)initGoogleEnable
{
    arrayActivities = [NSMutableArray array];
    
    GPPSignIn *signIn = [GPPSignIn sharedInstance];
    signIn.shouldFetchGooglePlusUser = YES;
    signIn.shouldFetchGoogleUserEmail = YES;  // Uncomment to get the user's email
    
    if(signIn.actions){
        for(NSString* appActivity in signIn.actions){
            [arrayActivities addObject:[appActivity lastPathComponent]];
        }
    }
    signIn.delegate = self;
    // signIn.clientID = ClientID_Google;
    
    
    // Uncomment one of these two statements for the scope you chose in the previous step
    
    //signIn.scopes = @[@"https://www.googleapis.com/auth/plus.login" ];
    // signIn.scopes = @[ @"profile" ];            // "profile" scope
    
    // Optional: declare signIn.actions, see "app activities"
    
    
}


#pragma mark - Twitter delegate
#pragma mark - Twitter engine Delegate
- (void)storeAccessToken:(NSString *)accessToken
{
    NSLog(@"Twitter Access Token: %@", accessToken);
    
    //Save user account to App Setting
    [[NSUserDefaults standardUserDefaults] setObject:accessToken forKey:Twitter_access_token];
}

- (NSString *)loadAccessToken
{
    return [[NSUserDefaults standardUserDefaults]objectForKey:Twitter_access_token];
}

#pragma mark - GPPSignInDelegate
-(void)adoptUserSettings
{
    GPPSignIn* signIn = [GPPSignIn sharedInstance];
    
    NSMutableArray *supportedAppActivities = [[NSMutableArray alloc] init];
    
    for (NSString *appActivity in arrayActivities) {
        NSString *schema =
        [NSString stringWithFormat:@"http://schemas.google.com/%@",
         appActivity];
        [supportedAppActivities addObject:schema];
    }
    signIn.actions = supportedAppActivities;
    
}

- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth
                   error: (NSError *) error {
    
    NSLog(@"Received error %@ and auth object %@",error, auth);
    User* userGet = [self getUserFromGoogle];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSLog(@"user id = %@",[userGet googleId]);
    
    [QBRequest userWithExternalID:[[userGet googleId] integerValue] successBlock:^(QBResponse *response, QBUUser *user) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        User* pUser = [User new];
        [pUser initWithData:user];
        [pUser saveSession];
        [self goToNShopHomePage];
        
    } errorBlock:^(QBResponse *response) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [self gotoCompleteRegVC:3 user:userGet];
    }];
    
    
    
}

- (IBAction)onSIgnOut:(id)sender {
    [[GPPSignIn sharedInstance] signOut];
}

- (void)showSignOutAlertViewWithConfirmationBlock:(void (^)(void))confirmationBlock
                                      cancelBlock:(void (^)(void))cancelBlock {
    if ([[GPPSignIn sharedInstance] authentication]) {
        
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"SignOut"
                                                            message:@""
                                                           delegate:self
                                                  cancelButtonTitle:@"cancel"
                                                  otherButtonTitles:@"Ok", nil];
        [alertView show];
    }
}
-(User*)getUserFromGoogle//10556150
{
    GTLPlusPerson* person  = [GPPSignIn sharedInstance].googlePlusUser;
    if(person == nil)
    {
        return nil;
    }
    User* user = [User new];
    NSString* googleID = [[Service sharedService] getUserIdFromGoogle:person.identifier];
    
    [user setGoogleId:googleID];
    [user setLogin:person.displayName];
    [user setUrlAvata:person.image.url];
    [user setEmail:[GPPSignIn sharedInstance].userEmail];
    [user setGender:person.gender];
    
    return user;
}

#pragma mark - PwdForgotten Delegate
-(void)onResetPwdSuccess
{
    UIAlertView* alertView =[[UIAlertView alloc] init];
    [alertView setTitle:@"Success to reset your password"];
    [alertView setMessage:@"Check your email"];
    [alertView addButtonWithTitle:@"Ok"];
    [alertView show];
    
}
#pragma mark - From UserName Forgottern Delegat
-(void)onResetPwdSuccessFromUserName
{
    UIAlertView* alertView =[[UIAlertView alloc] init];
    [alertView setTitle:@"Success to reset your password"];
    [alertView setMessage:@"Check your email"];
    [alertView addButtonWithTitle:@"Ok"];
    [alertView show];
}
#pragma mark - From Socail Register Delegate
-(void)onSuccessFromSocial
{
    UIAlertView* alertView =[[UIAlertView alloc] init];
    [alertView setTitle:@"Success to reset your password"];
    [alertView setMessage:@"Check your email"];
    [alertView addButtonWithTitle:@"Ok"];
    [alertView show];
}
@end