//
//  SocialRegisterVC.m
//  SnSChat
//
//  Created by Passion on 3/12/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import "SocialRegisterVC.h"
#import "UIImage+animatedGIF.h"
#import "Service.h"
#import "Constants.h"
#import "TermsAndConditionVC.h"
#import "DateSelPickerVC.h"
#import "ComboCell.h"
#import "RegisterVC.h"
#import "BrowserHomePageVC.h"
#import "MBProgressHUD.h"

@interface SocialRegisterVC ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UITextField *text_birthday;

@property (weak, nonatomic) IBOutlet UITextField *text_gender;

@property (weak, nonatomic) IBOutlet UIButton *bnt_CheckTerms;
@property (weak, nonatomic) IBOutlet UIButton *bnt_combo;

@property(retain,nonatomic) UITableView* tableView;
@property(retain,nonatomic) NSMutableArray* arrayDataSource;

@property(retain,nonatomic) DateSelPickerVC* vcDatePicker;
@end

@implementation SocialRegisterVC

-(id)initVC:(NSInteger)typeSocial userReg:(User *)user{
    self = [super initWithNibName:@"SocialRegisterVC" bundle:[NSBundle mainBundle]];
    if (self) {
      
        
        nStateFromSocial = typeSocial;
        sUser = [User cloneObjectWith:user];
        sUser.delegate = self;
        
        NSLog((@"%@",[sUser email]));
        
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.tableView registerNib:[UINib nibWithNibName:@"ComboCell" bundle:nil] forCellReuseIdentifier:@"ComboCell"];
    [self initComboBox];
    self.vcDatePicker = [[DateSelPickerVC alloc] initVC];
    
    [self addChildViewController:self.vcDatePicker];
    [self.view addSubview:self.vcDatePicker.view];
    self.vcDatePicker.delegate = self;
    
    self.vcDatePicker.view.hidden = true;
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [self initControls];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
#pragma mark - touch events
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}
-(void)keyBoardResign
{
    
    
}
#pragma mark - textfield delegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if(textField == self.text_gender)
    {
        [self comboChange];
        return false;
    }
    if(textField == self.text_birthday)
    {
        [self showDateSelVC];
        return false;
    }
   
//    CGRect frameTextField = [textField frame];
//    CGPoint point = frameTextField.origin;
//    CGPoint pos = [textField convertPoint:point fromView:self.view];
//    pos = CGPointMake(-pos.x, -pos.y);
//    CGSize sizeDevice = [[Service sharedService] sizeDevice];
//    CGFloat height = sizeDevice.height - pos.y - frameTextField.size.height;
//    CGFloat heightKeyboard = (CGFloat)KEYBOARD_HEIGHT_IPAD;
//    if(height < heightKeyboard + 40)
//    {
//        CGRect frameBnt = [self.bnt_signUp frame];
//        
//        CGFloat height = sizeDevice.height - frameBnt.origin.y - frameBnt.size.height;
//        UpView = heightKeyboard - height + 30;
//        
//        [self ScrollUp];
//    }
    return true;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self ScrollDown];
    [self keyBoardResign];
    return true;
}



#pragma mark - button click
- (IBAction)onClickButton:(id)sender {
    NSInteger tag = [sender tag];
    
    switch (tag) {
        case 1:
            [self comboChange];
            break;
        case 2:
            [self termsAndConditions];
            break;
        case 3:
            [self checkButton];
            break;
        case 4:
            [self onBack];
            break;
       
        default:
            break;
    }
}

- (IBAction)onSignUp:(id)sender {
    if([self isValidate]==0)
    {
        [sUser setBirthday:[self.text_birthday text]];
        [sUser setGender:[self.text_gender text]];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [sUser registerToQBFromSocial:nStateFromSocial];
        
    }
    else if([self isValidate] == 3)
    {
        UIAlertView* viewAlert;
        viewAlert = [[UIAlertView alloc] init];
        [viewAlert setTitle:@"Agree with the terms and conditions"];
        [viewAlert addButtonWithTitle:@"Ok"];
        [viewAlert show];
        return;
    }
    else if([self isValidate] == 2)
    {
        UIAlertView* viewAlert;
        viewAlert = [[UIAlertView alloc] init];

        [viewAlert setTitle:@"Select your gender"];
        [viewAlert addButtonWithTitle:@"Ok"];
        [viewAlert show];
        return;
    }
    else if([self isValidate] == 1)
    {
        UIAlertView* viewAlert;
        viewAlert = [[UIAlertView alloc] init];

        [viewAlert setTitle:@"Please input your birthday"];
        [viewAlert addButtonWithTitle:@"Ok"];
        [viewAlert show];
        return;
    }
    
   
    
}

- (IBAction)onCalendar:(id)sender {
    [self showDateSelVC];
}

- (IBAction)onTermsAndCondition:(id)sender {
    TermsAndConditionVC* vcWeb =[[TermsAndConditionVC alloc] initWithLocalUrl:@"Shopnsocial Terms and Conditions HTML 09032015" title:@"Terms & Conditions"];
    [self.navigationController pushViewController:vcWeb animated:YES];
}

- (IBAction)onPrivacyPolicies:(id)sender {
    TermsAndConditionVC* vcWeb =[[TermsAndConditionVC alloc] initWithLocalUrl:@"Privacy Policy HTML" title:@"Privacy Policies"];
    [self.navigationController pushViewController:vcWeb animated:YES];
}



-(void)onBack
{
    [self.navigationController popViewControllerAnimated:NO];
    
}

-(void)termsAndConditions
{
    TermsAndConditionVC* vcWeb =[[TermsAndConditionVC alloc] initWithLocalUrl:@"Shopnsocial Terms and Conditions HTML 09032015" title:@"Terms & Conditions"];
    [self.navigationController pushViewController:vcWeb animated:YES];
}
-(void)checkButton
{
    self.bnt_CheckTerms.selected = !self.bnt_CheckTerms.selected;
}

#pragma mark - user define
-(void)initControls
{
    IsSrollUp = false;
    
     self.text_birthday.delegate = self;
    if([sUser birthday] !=nil)
    {
        [self.text_birthday setText:[sUser birthday]];
    }
    else
    {
        [self.text_birthday setText:@""];
    }
    self.text_gender.delegate = self;

    if([sUser gender] != nil)
    {
        self.text_gender.text = [sUser gender];
    }
    else
    {
        self.text_gender.text = @"";

    }
    
   
    
    
}
-(void)initComboBox
{
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.arrayDataSource = [NSMutableArray array];
    [self.arrayDataSource addObject:@"male"];
    [self.arrayDataSource addObject:@"female"];
    [self.tableView reloadData];
    [self.tableView setSeparatorColor:[UIColor colorWithRed:255 green:255 blue:255 alpha:1]];
    [self.tableView setBackgroundColor:[UIColor colorWithRed:245 green:245 blue:245 alpha:1]];
    [self.viewComboBox addSubview:self.tableView];
    self.tableView.hidden = true;
    self.tableView.bounces = false;
    
    CGRect frameTable;
    CGRect frameGender = [self.viewComboBox frame];
    frameTable = CGRectMake(0, frameGender.size.height, frameGender.size.width , self.text_gender.frame.size.height * 2);
    [self.tableView setFrame:frameTable];
    
    
}
-(NSInteger)isValidate
{
    
    
    if([[self.text_birthday text] isEqualToString:@""])
    {
        
        return 1;
    }
  
  
    if([[self.text_gender text] isEqualToString:@""])
    {
       
        return 2;
    }
    if(self.bnt_CheckTerms.selected == false)
    {
        
        return 3;
    }
    return 0;
    
}
-(void)ScrollUp
{
    if(IsSrollUp == NO)
    {
        [UIView animateWithDuration:0.3 animations:^{
            CGRect frame = self.view.frame;
            frame.origin.y = frame.origin.y - UpView;
            [self.view setFrame:frame];
            IsSrollUp = true;
        }];
    }
}
-(void)ScrollDown
{
    if (IsSrollUp == YES) {
        [UIView animateWithDuration:0.3 animations:^{
            CGRect frame = self.view.frame;
            frame.origin.y = frame.origin.y + UpView;
            [self.view setFrame:frame];
            IsSrollUp = false;
        }];
    }
}
-(void)comboChange
{
    
    [self.tableView reloadData];
    if(self.bnt_combo.selected == false)
    {
        
        [UIView animateWithDuration: 0.2f delay:0.1f options:UIViewAnimationCurveEaseOut animations:^{
            CGRect frame = self.viewComboBox.frame;
            CGFloat height = frame.size.height * 3.0f;
            frame.size.height = height;
            [self.viewComboBox setFrame:frame];
            
            
        }completion:^(BOOL finished){
            NSLog(@"Done!");
            self.tableView.hidden = false;
            self.bnt_combo.selected = !self.bnt_combo.selected;
        }];
        
    }
    else{
        
        [UIView animateWithDuration: 0.2f delay:0.1f options:UIViewAnimationCurveEaseOut animations:^{
            CGRect frame = self.viewComboBox.frame;
            CGFloat height = frame.size.height / 3.0f;
            frame.size.height = height;
            [self.viewComboBox setFrame:frame];
            self.bnt_combo.selected = !self.bnt_combo.selected;
            
        }completion:^(BOOL finished){
            NSLog(@"Done!");
            self.tableView.hidden = true;
            
        }];
        
    }

}

-(void)showDateSelVC
{
    CGRect frameDatePicker = [self.vcDatePicker.view frame];
    CGSize sizeDevice = [[Service sharedService] sizeDevice];
    
    frameDatePicker = CGRectMake((sizeDevice.width-frameDatePicker.size.width)/2.0f, (sizeDevice.height-frameDatePicker.size.height)/2.0f, frameDatePicker.size.width, frameDatePicker.size.height);
    [self.vcDatePicker.view setFrame:frameDatePicker];
    [self.vcDatePicker.view setHidden:NO];
}


#pragma DatePickerSel Delegate
-(void)onDatePicker:(NSString *)dateString
{
    self.text_birthday.text = dateString;
    [self.vcDatePicker.view setHidden:YES];
    
}

#pragma mark - tableview delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.text_gender.frame.size.height;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ComboCell* cell = [[[NSBundle mainBundle] loadNibNamed:@"ComboCell" owner:self options:nil] objectAtIndex:0];
    NSString* str = [self.arrayDataSource objectAtIndex:indexPath.row];
    cell.text_Label.text = str;
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* str = [self.arrayDataSource objectAtIndex:indexPath.row];
    self.text_gender.text = str;
    [self comboChange];
}

#pragma mark - User delgate;
-(void)registerToQuickBlox:(BOOL)success object:(id)user
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    if(success == YES)
    {
        UIAlertView* alertView = [[UIAlertView alloc] init];
        [alertView setTitle:@"Success to sign up"];
        [alertView addButtonWithTitle:@"Ok"];
        alertView.delegate = self;
        [alertView setTag:2];
        [alertView show];
    }
    else
    {
        UIAlertView* alertView = [[UIAlertView alloc] init];
        [alertView setTitle:@"Fail to sign up"];
        [alertView setMessage:@"Will you try to sign up again?"];
        [alertView addButtonWithTitle:@"Yes"];
        [alertView addButtonWithTitle:@"No"];
        alertView.delegate = self;
        alertView.tag = 1;
        [alertView show];
        
    }
}

#pragma mark - alerttView Delgate;
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSInteger tag = alertView.tag;
    if(tag == 1)
    {
        NSString* titleOfButton = [alertView buttonTitleAtIndex:buttonIndex];
        if([titleOfButton isEqualToString:@"Yes"])
        {
            BOOL isConnectedToGoogle = [[Service sharedService] isConectedToGoogle];
            
            if(isConnectedToGoogle == false)
            {
                UIAlertView* alertView = [[UIAlertView alloc] init];
                [alertView setTitle:@"No Connected to the Internet."];
                [alertView addButtonWithTitle:@"Yes"];
                alertView.delegate = self;
                alertView.tag = 2;
                [alertView show];
                return;
            }
            else
            {
                [sUser registerToQuickBlox];
            }
        }
        else
        {
            //go to register with social n account.
            RegisterVC * vcRegister = [[RegisterVC alloc] initVC];
            vcRegister.delegate = self;
            [self.navigationController pushViewController:vcRegister animated:YES];
            
        }
    }
    else if(tag == 2)
    {
        BrowserHomePageVC* vcBrowser = [[BrowserHomePageVC alloc] initVC];
        [self.navigationController pushViewController:vcBrowser animated:YES];
        
    }
}

#pragma mark - register delegate
-(void)onSuccess
{
    [self.delegate onSuccessFromSocial];
    [self.navigationController popViewControllerAnimated:YES];
}
@end
