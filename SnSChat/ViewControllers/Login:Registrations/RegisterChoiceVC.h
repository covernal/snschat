//
//  RegisterChoiceVC.h
//  SnSChat
//
//  Created by Passion on 3/12/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RegisterVC.h"
#import "RoundBorderView.h"

@interface RegisterChoiceVC : UIViewController<RegisterVCDelegate>
{
    NSInteger nState;
}

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet RoundBorderView *viewFailReport;


-(id)initVC;
+(id)vcLogin;
- (IBAction)onButtonClick:(id)sender;

@end
