//
//  AgreeTermCondVC.m
//  SnSChat
//
//  Created by Passion on 3/18/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import "AgreeTermCondVC.h"
#import "BrowserHomePageVC.h"
#import "TermsAndConditionVC.h"

@interface AgreeTermCondVC ()

@end

@implementation AgreeTermCondVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

-(id)initVC
{
    self = [self initWithNibName:@"AgreeTermCondVC" bundle:[NSBundle mainBundle]];
    if(self)
    {
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onClickAgree:(id)sender {
    BrowserHomePageVC* vcBrowser =[[BrowserHomePageVC alloc] initVC];
    [self.navigationController pushViewController:vcBrowser animated:YES];
    
}

- (IBAction)onClickCancel:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onClickTermsConditions:(id)sender {
    TermsAndConditionVC* vcWeb =[[TermsAndConditionVC alloc] initWithLocalUrl:@"Shopnsocial Terms and Conditions HTML 09032015" title:@"Terms & Conditions"];
    [self.navigationController pushViewController:vcWeb animated:YES];
}

@end
