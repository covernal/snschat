//
//  WebVC.h
//  DYB
//
//  Created by Passion on 2/24/15.
//  Copyright (c) 2015 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "GifAniView.h"
#import "EGORefreshTableHeaderView.h"

@protocol WebViewDelegate <NSObject>

-(void)onBackFromWebView;

@end

@interface WebVC : UIViewController<UIWebViewDelegate, UIScrollViewDelegate, EGORefreshTableHeaderDelegate>
{
    //
    EGORefreshTableHeaderView * _refreshHeaderView;
    //
    BOOL _reloading;
}

@property (weak, nonatomic) IBOutlet UIWebView *uiWebView;
@property (weak, nonatomic) NSString* url;
@property (weak, nonatomic) IBOutlet GifAniView *img_bkg;

@property( weak, nonatomic) id<WebViewDelegate> delegate;
@property(weak,nonatomic) MBProgressHUD* hud;



- (IBAction)onBack:(UIButton *)sender;

- (id)initWithUrl:(NSString*)urlWeb;
-(id)initWithLocalUrl:(NSString*)urlLocal;
@end
