//
//  UnForgottenVC.h
//  SnSChat
//
//  Created by Passion on 3/13/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GifAniView.h"
#import "PwdForgottenVC.h"

@protocol UnForgottenVCDelegate <NSObject>

@optional

-(void)onResetPwdSuccessFromUserName;

@end

@interface UnForgottenVC : UIViewController<PwdForgottenVCDelegate>

@property (weak, nonatomic) IBOutlet UIButton *bnt_Emal;
@property (weak, nonatomic) IBOutlet GifAniView *img_bkg;
@property(weak,nonatomic) id<UnForgottenVCDelegate> delegate;

-(id)initVC:(NSString*)strEmail;
@end
