//
//  GLoginVC.h
//  SnSChat
//
//  Created by Passion on 3/13/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GifAniView.h"
#import "User.h"
@interface GLoginVC : UIViewController<UITextFieldDelegate>
{
    BOOL IsSrollUp;
    CGFloat UpView;
    User* loginedUser;
}
@property (weak, nonatomic) IBOutlet UITextField *text_Password;
@property (weak, nonatomic) IBOutlet UITextField *text_ConfirmPwd;

@property (weak, nonatomic) IBOutlet UIButton *bnt_NewPwdCreate;
@property (weak, nonatomic) IBOutlet GifAniView *img_bkg;

-(id)initVC:(User*)userLogined;

@end
