//
//  RegisterVC.h
//  SnSChat
//
//  Created by Passion on 3/11/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GifAniView.h"
#import "DateSelPickerVC.h"
#import "User.h"
@protocol RegisterVCDelegate <NSObject>

@optional
-(void)onFailRegister;
-(void)onSuccess;

@end

@interface RegisterVC : UIViewController<UITextFieldDelegate ,UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,DateSelDelegate,UserDelegate>
{
    BOOL IsSrollUp;
    CGFloat UpView;
    
}
@property (weak, nonatomic) IBOutlet GifAniView *img_bkg;

@property(weak ,nonatomic) id<RegisterVCDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITextField *text_UserName;
@property (weak, nonatomic) IBOutlet UITextField *text_Email;

@property (weak, nonatomic) IBOutlet UITextField *text_Password;
@property (weak, nonatomic) IBOutlet UITextField *text_Confirmpass;
@property (weak, nonatomic) IBOutlet UITextField *text_birthday;

@property (weak, nonatomic) IBOutlet UITextField *text_Gender;
@property (weak, nonatomic) IBOutlet UIButton *bnt_AgreeCheck;
@property (weak, nonatomic) IBOutlet UIButton *bnt_combo;

@property (weak, nonatomic) IBOutlet UIView *viewComboBox;

- (IBAction)onClickButton:(id)sender;

-(id)initVC;

@end
