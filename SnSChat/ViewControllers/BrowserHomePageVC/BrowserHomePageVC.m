//
//  BrowserHomePageVC.m
//  SnSChat
//
//  Created by Passion on 3/15/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import "BrowserHomePageVC.h"

@interface BrowserHomePageVC ()

@end

@implementation BrowserHomePageVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

-(id)initVC
{
    self = [self initWithNibName:@"BrowserHomePageVC" bundle:[NSBundle mainBundle]];
    if(self)
    {
       
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - button click
- (IBAction)onClickPlus:(id)sender {
    
}

- (IBAction)onClickChat:(id)sender {
    
}

- (IBAction)onClickSlide:(id)sender {
    
}
@end
