//
//  BrowserHomePageVC.h
//  SnSChat
//
//  Created by Passion on 3/15/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrowserHomePageVC : UIViewController


@property (weak, nonatomic) IBOutlet UIView *viewBase;
-(id)initVC;
@end
