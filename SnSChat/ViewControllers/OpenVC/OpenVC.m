//
//  OpenVC.m
//  SnSChat
//
//  Created by Passion on 3/11/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import "OpenVC.h"
#import "UIImage+animatedGIF.h"
#import "LoginVC.h"
#import "RegisterChoiceVC.h"
#import "TermsAndConditionVC.h"
@interface OpenVC ()
@property (weak, nonatomic) IBOutlet UIImageView *imageViewBkg;



- (IBAction)onClickButton:(id)sender;

@end

@implementation OpenVC

+(id)vcLogin{
    return [[OpenVC alloc] initWithNibName:@"OpenVC" bundle:[NSBundle mainBundle]];
}

-(id)initVC{
    self = [super initWithNibName:@"OpenVC" bundle:[NSBundle mainBundle]];
    if (self) {
        
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
}

-(void)viewWillAppear:(BOOL)animated{
    [self initControls];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}
#pragma user define
-(void)loginPage
{
    LoginVC* vcLogin = [[LoginVC alloc] initVC];
    [self.navigationController pushViewController:vcLogin animated:YES];
    
}
-(void)registerPage
{
    RegisterChoiceVC* vcRegisterChoice = [[RegisterChoiceVC alloc] initVC];
    [self.navigationController pushViewController:vcRegisterChoice animated:YES];
}
-(void)termsAndConditions
{
    
}


- (IBAction)onClickButton:(id)sender {
    NSInteger tag = [sender tag];
    switch (tag) {
        case 1:
            [self loginPage];
            break;
        case 2:
            [self registerPage];
            break;
        case 3:
            [self termsAndConditions];
            break;
        
        default:
            break;
    }
}
- (IBAction)onTermsAndConditions:(id)sender {
    TermsAndConditionVC* vcWeb =[[TermsAndConditionVC alloc] initWithLocalUrl:@"Shopnsocial Terms and Conditions HTML 09032015" title:@"Terms & Conditions"];
    [self.navigationController pushViewController:vcWeb animated:YES];
}
- (IBAction)onPrivacyPolicies:(id)sender {
    TermsAndConditionVC* vcWeb =[[TermsAndConditionVC alloc] initWithLocalUrl:@"Privacy Policy HTML" title:@"Privacy Policies"];
    [self.navigationController pushViewController:vcWeb animated:YES];
}

#pragma Bkg Setting
-(void)initControls
{
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"Loading-Image-Smaller" withExtension:@"gif"];
    self.imageViewBkg.image = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url]];
    
}
@end
