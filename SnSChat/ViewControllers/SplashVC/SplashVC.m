//
//  SplashVC.m
//  SnSChat
//
//  Created by Passion on 3/11/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import "SplashVC.h"
#import "MBProgressHUD.h"
#import "UIImage+animatedGIF.h"
#import "AppSession.h"

#import "LoginVC.h"
#import "User.h"
#import "MBProgressHUD.h"
#import "BrowserHomePageVC.h"
#import "Constants.h"
#import "GLoginVC.h"
@interface SplashVC ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) MBProgressHUD* hud;

@end

@implementation SplashVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

-(id)initWIthDelayTime:(CGFloat)delayTime
{
    self = [self initWithNibName:@"SplashVC" bundle:[NSBundle mainBundle]];
    if(self)
    {
        delay = delayTime;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [self initControls];
    
    
    [self performSelector:@selector(loadingNextView)
               withObject:nil afterDelay:1.5f];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark - user define
-(void)initControls
{
    [self initTwitter];
}

-(BOOL)checkSession
{
    
    return [[AppSession sharedAppSession] isLogined];
}

- (void)loadingNextView{
    
    LoginVC* vcOpen = [[LoginVC alloc] initVC];
    [self.navigationController pushViewController:vcOpen animated:YES];
    
//    if([self checkSession] == NO)
//    {
//        LoginVC* vcOpen = [[LoginVC alloc] initVC];
//        [self.navigationController pushViewController:vcOpen animated:YES];
//        
//    }
//    else
//    {
//        //After Logging with Generated Password
//        User* userLogined = [User retriveSessionUser];
//        if(userLogined == nil)
//        {
//            
//        }
//        else
//        {
//            [[AppSession sharedAppSession] setUserLogined:userLogined];
//            GLoginVC* vcGlogin =[[GLoginVC alloc] initVC:userLogined];
//            [self.navigationController pushViewController:vcGlogin animated:YES];
//            
////            BrowserHomePageVC* vcBrowser = [[BrowserHomePageVC alloc] initVC];
////            [self.navigationController pushViewController:vcBrowser animated:YES];
//        }
//        
//        
//        
////        GLoginVC* vcGLogin = [[GLoginVC alloc] init];
////        [self.navigationController pushViewController:vcGLogin animated:YES];
//    }

}

#pragma mark - Twitter


-(void)initTwitter
{
    [[FHSTwitterEngine sharedEngine]permanentlySetConsumerKey:Twitter_consumer_key andSecret:Twitter_consumer_secret];
    [[FHSTwitterEngine sharedEngine]setDelegate:self];
   
}

#pragma mark - Twitter engine Delegate
- (void)storeAccessToken:(NSString *)accessToken
{
    NSLog(@"Twitter Access Token: %@", accessToken);
    
    //Save user account to App Setting
    [[NSUserDefaults standardUserDefaults] setObject:accessToken forKey:Twitter_access_token];
}

- (NSString *)loadAccessToken
{
    return [[NSUserDefaults standardUserDefaults]objectForKey:Twitter_access_token];
}
@end
