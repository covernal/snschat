//
//  SplashVC.h
//  SnSChat
//
//  Created by Passion on 3/11/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import  <Twitter/Twitter.h>
#import <Social/Social.h>
#import "FHSTwitterEngine.h"

@interface SplashVC : UIViewController<FHSTwitterEngineAccessTokenDelegate>
{
    CGFloat delay;
}


-(id)initWIthDelayTime:(CGFloat) delayTime;

@end
