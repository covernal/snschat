//
//  Service.m
//  SnSChat
//
//  Created by Passion on 3/12/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import "Service.h"
#import "Reachability.h"

static Service* service = nil;
@implementation Service
+(id)sharedService
{
    if(service == nil)
    {
        service = [[Service alloc] init];
    }
    return  service;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        service = self;
    }
    return self;
}

-(CGSize)sizeDevice
{
    CGRect frame = [[UIScreen mainScreen] bounds];
    CGSize size = CGSizeMake(frame.size.width, frame.size.height);
    return  size;
}
#pragma mark - check connecting to the google.
-(BOOL)isConectedToGoogle
{
    BOOL flag;
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        NSLog(@"There IS NO internet connection");
        flag = NO;
    } else {
        NSLog(@"There IS internet connection");
        flag = YES;
    }
    return flag;
}
-(id)alertView:(NSString *)title msg:(NSString *)msg buttons:(NSArray *)arrayBntStrings taget:(id)target
{
    UIAlertView* alertView = [[UIAlertView alloc] init];
    [alertView setTitle:title];
    [alertView setMessage:msg];
    for (NSString* item in arrayBntStrings) {
        [alertView addButtonWithTitle:item];
    }
    alertView.delegate = target;
    
    return alertView;
}
#pragma mark - check email address.
- (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}
//3083780350-tvxq27FM30TD9Phakl4zlhwe53ANakPyCDY5AhI&oauth_token_secret=rkGKe4fZQvWTCUIZZRp1qDwnhvbodiZQ4JfscyiRHociT&user_id=3083780350&screen_name=ShopnSocial
#pragma mark - Get user id from access token in twitter

-(NSString*)getUserIdFromAccessToken:(NSString*)tokenAccess
{
    NSMutableArray* array = [tokenAccess componentsSeparatedByString:@"&"];
    NSString* subPart = [array objectAtIndex:2];
    array = [subPart componentsSeparatedByString:@"="];
    NSString* user_id = [array objectAtIndex:1];
    return user_id;
}

#pragma mark - Get id from Google id.
-(NSString*)getUserIdFromGoogle:(NSString*)idGoogle
{
    NSString* prefix = [idGoogle substringToIndex:[idGoogle length]/4];
    
    NSString* subTail = [idGoogle substringFromIndex:[idGoogle length]-3];
    NSInteger sum = 0;
    
    for(int i = 0;i<[subTail length];i++)
    {
        sum = sum + (NSInteger)[idGoogle characterAtIndex:i];
    }
    
    NSString* googlId = [NSString stringWithFormat:@"%@%d",prefix,sum];
    
    return googlId;
}
@end
