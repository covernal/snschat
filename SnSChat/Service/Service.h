//
//  Service.h
//  SnSChat
//
//  Created by Passion on 3/12/15.
//  Copyright (c) 2015 Kite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Service : NSObject
+(id)sharedService;
-(CGSize)sizeDevice;
-(BOOL)isConectedToGoogle;

-(id)alertView:(NSString*)title msg:(NSString*)msg buttons:(NSArray*)arrayBntStrings taget:(id)target;
- (BOOL) validateEmail: (NSString *) candidate ;
-(BOOL)isConectedToGoogle;
-(NSString*)getUserIdFromAccessToken:(NSString*)tokenAccess;
-(NSString*)getUserIdFromGoogle:(NSString*)idGoogle;
@end
